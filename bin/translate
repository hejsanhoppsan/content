#!/usr/bin/env node
/*
 * Copyright (c) 2018-2021 29k International AB
 */
'use strict';
import { v3 } from '@google-cloud/translate';
import {
  always,
  compose,
  forEach,
  head,
  is,
  isNil,
  keys,
  map,
  memoizeWith,
  prop,
  uniq,
} from 'ramda';

const { TranslationServiceClient } = v3;

const SKIPLIST = new Set([
  'color',
  'component',
  'conversationId',
  'duration',
  'id',
  'imageUri',
  'instagramHandle',
  'params',
  'root',
  'screenType',
  'sectionId',
  'segments',
  'tags',
  'topImageUri',
  'type',
  'uri',
  'url',
  'videoUri',
]);

// slurp reads a stream from start to end before resolving.
const slurp = (stream) =>
  new Promise((resolve) => {
    const chunks = [];
    stream
      .on('data', (chunk) => chunks.push(chunk))
      .on('end', () => resolve(chunks.join('')))
      .setEncoding('utf8');
  });

// translationServiceClient lazily constructs a translation service client.
//
// If no translations are needed in a file, skipping loading of this will
// improve performance by a lot.
const translationServiceClient = memoizeWith(always(1), () => {
  return new TranslationServiceClient();
});

const googleAuthLibrary = memoizeWith(always(1), () => {
  import { GoogleAuth } from 'google-auth-library';
  return new GoogleAuth();
});

const getTranslatedText = prop('translatedText');
const getTranslations = prop('translations');

// createTranslator offer translation of text from source to target language.
const createTranslator = (sourceLang, targetLang) => async (text) => {
  const projectId = await googleAuthLibrary().getProjectId();
  const [response] = await translationServiceClient().translateText({
    parent: `projects/${projectId}/locations/global`,
    contents: [text],
    mimeType: 'text/plain',
    sourceLanguageCode: sourceLang,
    targetLanguageCode: targetLang,
  });
  return compose(getTranslatedText, head, getTranslations)(response);
};

// translatePairs translates structures from a source language
//
// It iterates over all nodes from the source language, dynamically
// constructing the same structure for the target language.
//
// Any string found not present in the target language is translated.
const translatePairs = async (translator, from, to) => {
  // Try to preserve order primarily by destination and secondly by source
  const targetKeys = uniq([...keys(to), ...keys(from)]);
  const acc = is(Array, from) ? [] : {};
  forEach((key) => (acc[key] = null), targetKeys);

  const promises = map(async (key) => {
    const source = from[key];
    const target = to[key];
    if (SKIPLIST.has(key)) {
      acc[key] = source;
    } else if (is(Array, source)) {
      acc[key] = await translatePairs(translator, source, [...(target || [])]);
    } else if (is(Object, source)) {
      acc[key] = await translatePairs(translator, source, { ...target });
    } else if (is(String, source) && source && isNil(target)) {
      acc[key] = await translator(source);
    } else if (target) {
      acc[key] = target;
    } else {
      acc[key] = source;
    }
  }, targetKeys);
  await Promise.all(promises);
  return acc;
};

const translateContent = async (
  translator,
  content,
  sourceLang,
  targetLang,
) => ({
  ...content,
  [targetLang]: await translatePairs(
    translator,
    content[sourceLang],
    content[targetLang] || {},
  ),
});

const parseArguments = () =>
  require('yargs')
    .option('source', { describe: 'source language', default: 'en' })
    .option('target', { describe: 'target language', default: 'sv' }).argv;

const execute = async () => {
  const { source, target } = parseArguments();
  const data = await slurp(process.stdin);
  const content = JSON.parse(data);

  const translator = createTranslator(source, target);
  const translated = await translateContent(
    translator,
    content,
    source,
    target,
  );

  const json = JSON.stringify(translated, null, 2);
  process.stdout.write(json);
};

async function main() {
  try {
    await execute();
  } catch (error) {
    console.error(error);
    process.exit(127);
  }
}

main();
