#!/usr/bin/env node
/*
 * Copyright (c) 2018-2021 29k International AB
 */
import fs from 'fs';
import path from 'path';
import glob from 'glob';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import createRequire from '../lib/require.cjs';

const cjsRequire = createRequire(import.meta.url);

const parseArguments = () =>
  yargs(hideBin(process.argv))
    .option('source', { describe: 'source language', default: 'en' })
    .option('target', { describe: 'target language', default: 'sv' })
    .option('glob', {
      describe: 'glob style files to match',
      default: '**/*.json',
    })
    .option('force', {
      describe: 'force overwrite',
      boolean: true,
    }).argv;

const listFiles = (path) =>
  new Promise((resolve) => glob(path, null, (err, files) => resolve(files)));
const openFile = (path) => cjsRequire(path);
const writeFile = (path, content) =>
  fs.writeFileSync(path, JSON.stringify(content, null, 2));

const execute = async () => {
  const { source, target, glob, force } = parseArguments();

  const paths = await listFiles(path.resolve('../src/', glob));

  paths.forEach((path) => {
    const file = openFile(path);

    if (source in file && (!(target in file) || force)) {
      console.log(`copying ${source} to ${target} in ${path}`);

      const content = {
        ...file,
        [target]: file[source],
      };

      writeFile(path, content);
    } else {
      console.log(`${path} already copied or not translatable`);
    }
  });
};

async function main() {
  try {
    await execute();
  } catch (error) {
    console.error(error);
    process.exit(127);
  }
}

main();
