/*
 * Copyright (c) 2018-2021 29k International AB
 */
export default {
  /**
   * At the moment we use babel in Jest until ESM is fully supported:
   * https://github.com/facebook/jest/issues/9430
   * https://github.com/facebook/jest/pull/10976
   */
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  coverageReporters: ['text', 'cobertura'],
};
