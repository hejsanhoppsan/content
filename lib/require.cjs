const { dirname, resolve } = require('path');
const { fileURLToPath } = require('url');

const createRequire = (originPath) => (filePath) => {
  const dirPath = dirname(fileURLToPath(originPath));
  return require(resolve(dirPath, filePath));
};

module.exports = createRequire;
