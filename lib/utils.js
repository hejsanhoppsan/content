import { pipe, pluck, map, reduce } from 'ramda';

const groupById = reduce((acc, el) => ({ ...acc, [el.id]: el }), {});

const getContentLibForLang = (lang, lib = {}) =>
  map(pipe(pluck(lang), groupById), lib);

export { getContentLibForLang };
