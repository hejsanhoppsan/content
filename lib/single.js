import { applySpec, map, mapObjIndexed, chain } from 'ramda';

import { ExerciseCompleteButton, Button } from './cui/components/index.js';
import { TEMPLATES, preprocessSection } from './cui/index.js';
import { getContentLibForLang } from './utils.js';

const connectSections = (convId, singleType) => (section, idx, sections) => {
  const isLastSection = sections.length === idx + 1;
  const nextSectionId = sections[idx + 1] && sections[idx + 1].id;

  switch (section.type) {
    case TEMPLATES.SHARING_EXERCISE:
      return {
        ...section,
        nextSection: !isLastSection && nextSectionId,
      };

    case TEMPLATES.INTRODUCTION:
      if (idx !== 0) {
        throw new Error(
          'The Introduction section can only appear as the first section of the conversation',
        );
      }
      return {
        ...section,
        type: TEMPLATES.EDITORIAL_WITH_IMAGE,
        topImageUri:
          singleType === 'exercise'
            ? 'https://res.cloudinary.com/twentyninek/image/upload/v1628579304/Badges/Exercise_badge_w_whitespace.png'
            : 'https://res.cloudinary.com/twentyninek/image/upload/v1628579304/Badges/Meditation_badge_w_whitespace.png',
        content: section.text,
        buttons: [
          isLastSection
            ? ExerciseCompleteButton(section.buttonText)
            : Button(section.buttonText, convId, nextSectionId),
        ],
      };
    case TEMPLATES.EXERCISE:
    case TEMPLATES.MEDITATION:
    case TEMPLATES.EDITORIAL_WITH_IMAGE:
    case TEMPLATES.WRITING:
    case TEMPLATES.AUDIO:
    case TEMPLATES.VIDEO:
      return {
        ...section,
        buttons: [
          isLastSection
            ? ExerciseCompleteButton(section.buttonText)
            : Button(section.buttonText, convId, nextSectionId),
        ],
      };
    default:
      return section;
  }
};

const singleToConversation = (contentLib = {}) =>
  mapObjIndexed(({ sections = [], ...single }, lang) => {
    const contentLibForLang = getContentLibForLang(lang, contentLib);
    const conversationId = `conversation-${single.id}`;
    const sectionsWithButtons = sections.map(
      connectSections(conversationId, single.type),
    );

    return {
      ...single,
      id: conversationId,
      root: sections[0].id,
      sections: chain(
        preprocessSection(lang, conversationId, contentLibForLang),
        sectionsWithButtons,
      ),
    };
  });

/* eslint-disable no-unused-vars */
const singleToExercise = map(({ sections, ...single }) => ({
  ...single,
  conversations: [
    {
      title: single.name,
      conversationId: `conversation-${single.id}`,
    },
  ],
}));

const preprocessSingles = (contentLib) =>
  applySpec({
    exercises: map(singleToExercise),
    conversations: map(singleToConversation(contentLib)),
  });

export { preprocessSingles };
