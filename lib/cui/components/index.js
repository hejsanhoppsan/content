export const TextGroup = (...content) =>
  content[0] && {
    component: 'TextGroup',
    content,
  };

export const Text = (content) =>
  content && {
    component: 'Text',
    content,
  };

export const Guiding = (content) =>
  content && {
    component: 'Guiding',
    content,
  };

export const TextParagraphs = (content = '') => content.split('\n\n').map(Text);

export const Subtitle = (content) =>
  content && {
    component: 'Subtitle',
    content,
  };

export const Image = (url, props) =>
  url && {
    component: 'Image',
    content: {
      url,
    },
    props,
  };

export const Badge = (url, props) =>
  url && {
    component: 'Badge',
    content: {
      url,
    },
    ...(props ? { props } : {}),
  };

export const MiniBadge = (url) => ({
  component: 'MiniBadge',
  content: {
    url,
  },
});

export const FillViewport = () => ({
  fillViewport: true,
});

export const PushNotification = (title, body, when, target) => ({
  type: 'PushNotification',
  options: {
    when,
    title,
    body,
    target,
  },
});

export const Pause = (when, target) => ({
  type: 'Pause',
  options: {
    when,
    target,
  },
});

export const Heading = (content) =>
  content && Text(content[0] === '#' ? content : `# ${content}`);

export const ButtonGroup = (...content) =>
  content[0] && {
    component: 'ButtonGroup',
    content,
  };

export const Button = (content, conversationId, sectionId, params) =>
  content && {
    component: 'Button',
    content,
    conversationId,
    sectionId,
    ...(params ? { params } : {}),
  };

export const NextConversationButton = (content, conversationId, params) =>
  content && {
    component: 'NextConversationButton',
    content,
    conversationId,
    ...(params ? { params } : {}),
  };

export const CloseButton = (content) => ({
  component: 'CloseButton',
  content,
});

export const CheckPNsStatusButton = (
  content,
  conversationId,
  sectionId,
  params,
) => ({
  component: 'CheckPNsStatusButton',
  content,
  conversationId: conversationId,
  sectionId: sectionId,
  notDeterminedSectionId: params.notDeterminedSectionId,
  deniedSectionId: params.deniedSectionId,
});

export const RequestPNsButton = (
  content,
  conversationId,
  sectionId,
  params,
) => ({
  component: 'RequestPNsButton',
  content,
  conversationId,
  sectionId,
  deniedSectionId: params.deniedSectionId,
});

export const Section =
  (id, props, params) =>
  (content = [], task) => ({
    id,
    content,
    ...(task ? { task } : {}),
    // Section-scoped data
    ...(props ? { props } : {}),
    // TODO: props vs params – unify to keep only one
    ...(params ? { params } : {}),
  });

export const SubmitSharingButton = (content, data) => ({
  component: 'SubmitSharingButton',
  content,
  conversationId: data.conversationId,
  sectionId: data.sectionId,
  props: {
    key: data.key,
    category: data.category,
    question: data.question,
  },
});

export const ExerciseCarousel = (category, exampleText, questionKey) => ({
  component: 'ExerciseCarousel',
  props: {
    category,
    exampleText,
    questionKey,
  },
});

export const ExerciseCompleteButton = (content) => ({
  component: 'ExerciseCompleteButton',
  content,
});

export const LessonCompleteButton = (content, conversationId, props) => ({
  component: 'LessonCompleteButton',
  content,
  conversationId,
  props,
});

export const CompleteLessonButton = (content, conversationId) => ({
  component: 'CompleteLessonButton',
  content,
  conversationId,
});

export const Input = (data) => ({
  component: 'Input',
  props: {
    target: data.target,
    enableSkip: data.enableSkip,
    skipText: data.skipText,
    skipTarget: data.skipTarget,
    placeholder: data.placeholder,
    questionKey: data.questionKey,
  },
});

export const ExerciseReview = (data) => ({
  component: 'ExerciseReview',
  props: {
    key: data.key,
  },
});

export const AudioPlayer = (uri, duration) =>
  uri && {
    component: 'AudioPlayer',
    content: {
      uri,
      duration,
    },
  };

export const VideoPlayer = (videoUri, imageUri, subtitleUri, title, duration) =>
  videoUri && {
    component: 'VideoStories',
    content: [
      {
        videoUri,
        imageUri,
        subtitleUri,
        title,
        duration,
      },
    ],
  };

export const VideoStories = (videos = []) =>
  videos.length && {
    component: 'VideoStories',
    content: videos,
  };

export const Video = ({
  videoUri,
  imageUri,
  subtitleUri,
  title,
  duration,
}) => ({
  videoUri,
  imageUri,
  subtitleUri,
  title,
  duration,
});

export const CollapsibleList = (items = []) =>
  items.length && {
    component: 'CollapsibleList',
    items,
  };
