import { mapObjIndexed, chain, map } from 'ramda';

import audioTemplate from './templates/audio.js';
import editorialWithImageTemplate from './templates/editorialWithImage.js';
import sharingExerciseTemplate from './templates/sharingExercise.js';
import videoTemplate from './templates/video.js';
import writingTemplate from './templates/writing.js';
import microInterventionDayTemplate from './templates/microInterventionDay.js';
import meditationRef from './templates/meditationRef.js';
import exerciseRef from './templates/exerciseRef.js';

const TEMPLATES = {
  SHARING_EXERCISE: 'sharingExercise',
  EDITORIAL_WITH_IMAGE: 'editorialWithImage',
  WRITING: 'writing',
  AUDIO: 'audio',
  VIDEO: 'video',
  MICRO_INTERVENTION_DAY: 'microInterventionDay',
  MEDITATION: 'meditation',
  EXERCISE: 'exercise',
  INTRODUCTION: 'introduction',
};

const processConvByLang = mapObjIndexed(
  ({ sections = [], ...conversation }, lang) => ({
    ...conversation,
    sections: chain(preprocessSection(lang, conversation.id), sections),
  }),
);

// OBS! For now, we flatten the task array (that CMS outputs) into a single object
// Later, if we allow multiple tasks per section, we need to change the BE scheduler as well
const flattenSectionTaskArray = (section) => {
  const { task = [] } = section;
  return {
    ...section,
    ...(task.length ? { task: task[0] } : {}),
  };
};

const preprocessConversations = map(processConvByLang);
const preprocessSection =
  (lang, conversationId, content = {}) =>
  (section) => {
    const { id: sectionId, type } = section;
    const parent = { conversationId, sectionId, lang };

    switch (type) {
      case TEMPLATES.SHARING_EXERCISE:
        return sharingExerciseTemplate(parent, section);

      case TEMPLATES.EDITORIAL_WITH_IMAGE:
        return editorialWithImageTemplate(parent, section);

      case TEMPLATES.WRITING:
        return writingTemplate(parent, section);

      case TEMPLATES.AUDIO:
        return audioTemplate(parent, section);

      case TEMPLATES.VIDEO:
        return videoTemplate(parent, section);

      case TEMPLATES.MICRO_INTERVENTION_DAY:
        return microInterventionDayTemplate(
          { conversationId, sectionId, lang },
          section,
        );

      case TEMPLATES.MEDITATION:
        return meditationRef(parent, section, content.meditations);

      case TEMPLATES.EXERCISE:
        return exerciseRef(parent, section, content.exercises);

      default:
        return flattenSectionTaskArray(section);
    }
  };

export { preprocessConversations, preprocessSection, TEMPLATES };
