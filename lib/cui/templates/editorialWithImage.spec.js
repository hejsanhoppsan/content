import editorialWithImage from './editorialWithImage.js';

describe('editorialWithImage', () => {
  it('returns an object', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
    };
    const data = {
      topImageUri: 'nasa://milkyway',
      title: 'A title',
      content: 'The body text',
      buttons: [
        {
          component: 'Button',
          content: 'The button text',
          conversationId: 'some-conversation-id',
          sectionId: 'where-do-we-go-from-here',
        },
      ],
    };

    expect(editorialWithImage(parent, data)).toEqual({
      id: 'some-section-id',
      content: [
        {
          component: 'Image',
          content: { url: 'nasa://milkyway' },
          props: undefined,
        },
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: '# A title' },
            { component: 'Text', content: 'The body text' },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'The button text',
              conversationId: 'some-conversation-id',
              sectionId: 'where-do-we-go-from-here',
            },
          ],
        },
      ],
    });
  });
  it('throws if no buttons are specified', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
    };
    const data = {
      buttons: [],
    };

    expect(() => {
      editorialWithImage(parent, data);
    }).toThrow();
  });
});
