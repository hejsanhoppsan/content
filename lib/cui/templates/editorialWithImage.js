import {
  TextGroup,
  Heading,
  TextParagraphs,
  ButtonGroup,
  Section,
  Image,
  CollapsibleList,
} from '../components/index.js';

import { removeNilComponents } from '../utils.js';

const editorialWithImage = (
  { sectionId },
  { topImageUri, title, content, infoItems, buttons = [] },
) => {
  if (buttons.length < 1) {
    throw new Error(
      `[${sectionId}] Template editorialWithImage: 0 buttons will lead to dead ends`,
    );
  }

  return Section(sectionId)(
    removeNilComponents(
      Image(topImageUri),
      TextGroup(
        ...removeNilComponents(
          Heading(title),
          ...TextParagraphs(content),
          CollapsibleList(infoItems),
        ),
      ),
      ButtonGroup(...buttons),
    ),
  );
};

export default editorialWithImage;
