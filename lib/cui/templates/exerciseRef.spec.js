import exerciseRef from './exerciseRef.js';

describe('exerciseRef', () => {
  it('returns an object', () => {
    const parent = {
      sectionId: 'some-section-id',
    };
    const exercises = {
      'exercise-id': {
        name: 'name-value',
        description: 'description-value',
      },
    };
    const data = {
      refId: 'exercise-id',
      buttons: [
        {
          component: 'Button',
          content: 'Go go go',
          conversationId: 'some-conversation-id',
          sectionId: 'u-turn',
        },
      ],
    };

    expect(exerciseRef(parent, data, exercises)).toEqual({
      id: 'some-section-id',
      content: [
        {
          component: 'TextGroup',
          content: [
            {
              component: 'MiniBadge',
              content: {
                url: 'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
              },
            },
            {
              component: 'Text',
              content: '# name-value',
            },
            {
              component: 'Text',
              content: 'description-value',
            },
          ],
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Go go go',
              conversationId: 'some-conversation-id',
              sectionId: 'u-turn',
            },
          ],
        },
      ],
    });
  });
});
