import createRequire from '../../../lib/require.cjs';
import {
  TextGroup,
  Text,
  Guiding,
  ButtonGroup,
  Badge,
  MiniBadge,
  LessonCompleteButton,
  RequestPNsButton,
  CheckPNsStatusButton,
  CloseButton,
  Section,
  Pause,
  PushNotification,
  FillViewport,
} from '../components/index.js';
import sharingExerciseTemplate from './sharingExercise.js';
import * as questCheckIn from './questCheckIn.js';

const cjsRequire = createRequire(import.meta.url);
const translations = cjsRequire(
  '../../../src/ui/CUI.MicroInterventionDay.json',
);

const sectionIDs = {
  WELCOME: 'welcome',
  ALLOW_PNS: 'allowPNs',
  PNS_DENIED: 'PNsDenied',
  SCHEDULE_TASK: 'scheduleTask',
  TASK: 'task',
  SHARING_WALL: 'sharingWall',
  SKIP: 'skip',
  END_OF_DAY: 'endOfDay',
  END_OF_WEEK: 'endOfWeek',
};

const microInterventionDayTemplate = (parent, data) => {
  const { conversationId, lang } = parent;
  const {
    nextConversationId,
    initialDay,
    lessonlastDay,
    task,
    followUp,
    sharingWall,
  } = data;
  const t = translations[lang];

  const autoProgress = Pause(
    {
      days: 1,
      hours: 7,
      minutes: 30,
    },
    {
      conversationId: nextConversationId,
      sectionId: sectionIDs.SCHEDULE_TASK,
    },
  );

  return [
    Section(sectionIDs.WELCOME)([
      TextGroup(
        Text(t[sectionIDs.WELCOME].title__markdown),
        MiniBadge(
          'https://res.cloudinary.com/twentyninek/image/upload/v1617025399/Badges/quest_mini-badge_qgumki.png',
        ),
        Text(t[sectionIDs.WELCOME].quest__markdown),
        MiniBadge(
          'https://res.cloudinary.com/twentyninek/image/upload/v1617025399/Badges/check-in_mini-badge_jiebyc.png',
        ),
        Text(t[sectionIDs.WELCOME].checkIn__markdown),
        MiniBadge(
          'https://res.cloudinary.com/twentyninek/image/upload/v1617025399/Badges/share_mini-badge_zl2ugh.png',
        ),
        Text(t[sectionIDs.WELCOME].groupSharing__markdown),
      ),
      ButtonGroup(
        CheckPNsStatusButton(
          t[sectionIDs.WELCOME].start__button,
          conversationId,
          sectionIDs.SCHEDULE_TASK,
          {
            notDeterminedSectionId: sectionIDs.ALLOW_PNS,
            deniedSectionId: sectionIDs.PNS_DENIED,
          },
        ),
      ),
    ]),

    Section(sectionIDs.ALLOW_PNS)([
      TextGroup(
        Text(t[sectionIDs.ALLOW_PNS].title__markdown),
        Text(t[sectionIDs.ALLOW_PNS].text__markdown),
      ),
      ButtonGroup(
        RequestPNsButton(
          t[sectionIDs.ALLOW_PNS].enablePNs__button,
          conversationId,
          sectionIDs.SCHEDULE_TASK,
          {
            deniedSectionId: sectionIDs.PNS_DENIED,
          },
        ),
      ),
    ]),

    Section(sectionIDs.PNS_DENIED)([
      TextGroup(
        Text(t[sectionIDs.PNS_DENIED].title__markdown),
        Text(t[sectionIDs.PNS_DENIED].paragraph1__markdown),
        Text(t[sectionIDs.PNS_DENIED].paragraph2__markdown),
        Text(t[sectionIDs.PNS_DENIED].paragraph3__markdown),
      ),
      ButtonGroup(
        CheckPNsStatusButton(
          t[sectionIDs.PNS_DENIED].PNsTurnedOn__button,
          conversationId,
          sectionIDs.SCHEDULE_TASK,
          {
            notDeterminedSectionId: sectionIDs.ALLOW_PNS,
            deniedSectionId: sectionIDs.PNS_DENIED,
          },
        ),
      ),
    ]),

    Section(sectionIDs.SCHEDULE_TASK)(
      [
        TextGroup(
          Text(
            initialDay
              ? t[sectionIDs.SCHEDULE_TASK].titleInitialDay__markdown
              : t[sectionIDs.SCHEDULE_TASK].title__markdown,
          ),
          Text(
            initialDay
              ? t[sectionIDs.SCHEDULE_TASK].textInitialDay__markdown
              : t[sectionIDs.SCHEDULE_TASK].text__markdown,
          ),
        ),
        ButtonGroup(CloseButton(t[sectionIDs.SCHEDULE_TASK].confirm__button)),
      ],
      PushNotification(
        task.title,
        task.body,
        {
          days: task.days,
          hours: task.hours,
          minutes: task.minutes,
        },
        { conversationId, sectionId: sectionIDs.TASK },
      ),
    ),

    Section(
      sectionIDs.TASK,
      null,
      FillViewport(),
    )(
      [
        Badge(
          'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/quest_badge_gofzky.png',
        ),
        TextGroup(
          Text(t[sectionIDs.TASK].title__markdown),
          Text(task.body),
          MiniBadge(
            'https://res.cloudinary.com/twentyninek/image/upload/v1617025399/Badges/check-in_mini-badge_jiebyc.png',
          ),
          Guiding(t[sectionIDs.TASK].guiding__markdown),
        ),
        ButtonGroup(CloseButton(t[sectionIDs.TASK].confirm__button)),
      ],
      PushNotification(
        followUp.title,
        followUp.body,
        {
          days: followUp.days,
          hours: followUp.hours,
          minutes: followUp.minutes,
        },
        {
          conversationId,
          sectionId: questCheckIn.sectionIDs.FOLLOW_UP,
        },
      ),
    ),

    // Sharing Exercise template
    ...sharingExerciseTemplate(
      {
        ...parent,
        sectionId: sectionIDs.SHARING_WALL,
      },
      {
        ...data,
        categoryKey: `${sectionIDs.SHARING_WALL}-${task.id}`,
        question: sharingWall.question,
        exampleText: sharingWall.exampleText,
        nextConversation: conversationId,
        nextSection: questCheckIn.sectionIDs.Q1,
      },
    ),

    // Quest check-in template
    ...questCheckIn.template(
      {
        ...parent,
        nextConversationId,
      },
      {
        ...data,
        questCompleteSectionId: lessonlastDay
          ? sectionIDs.END_OF_WEEK
          : sectionIDs.END_OF_DAY,
        skipFollowUpSectionId: lessonlastDay
          ? sectionIDs.END_OF_WEEK
          : sectionIDs.SKIP,
        autoProgress: !lessonlastDay ? autoProgress : undefined,
        taskCompletedSectionId: sectionIDs.SHARING_WALL,
      },
    ),

    Section(sectionIDs.END_OF_WEEK)([
      TextGroup(
        Text(t[sectionIDs.END_OF_WEEK].title__markdown),
        Text(t[sectionIDs.END_OF_WEEK].text__markdown),
      ),
      ButtonGroup(
        LessonCompleteButton(
          t[sectionIDs.END_OF_WEEK].weekCompleted__button,
          conversationId,
        ),
      ),
    ]),

    // End of day section is only rendered when it's not the last day of the lesson
    ...(!lessonlastDay
      ? [
          Section(sectionIDs.END_OF_DAY)([
            TextGroup(
              Text(t[sectionIDs.END_OF_DAY].title__markdown),
              Text(t[sectionIDs.END_OF_DAY].paragraph__markdown),
            ),
            ButtonGroup(CloseButton(t[sectionIDs.END_OF_DAY].confirm__button)),
          ]),
        ]
      : []),

    // Skip section is only rendered when it's not the last day of the lesson
    ...(!lessonlastDay
      ? [
          Section(sectionIDs.SKIP)([
            TextGroup(Text(t[sectionIDs.SKIP].text__markdown)),
            ButtonGroup(CloseButton(t[sectionIDs.SKIP].confirm__button)),
          ]),
        ]
      : []),
  ];
};

export default microInterventionDayTemplate;
