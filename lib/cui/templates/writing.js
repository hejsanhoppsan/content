import {
  TextGroup,
  Heading,
  TextParagraphs,
  ButtonGroup,
  Subtitle,
  Section,
  Badge,
} from '../components/index.js';

import { removeNilComponents } from '../utils.js';

const DEFAULT_EXERCISE_BADGE =
  'https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1617023623/Badges/exercise_badge_ltbfuq.png';

const writing = (
  { sectionId },
  {
    title,
    time,
    question,
    shortDescription,
    longDescription,
    buttons = [],
    topImageUri = DEFAULT_EXERCISE_BADGE,
  },
) =>
  Section(sectionId)(
    removeNilComponents(
      Badge(topImageUri),
      TextGroup(
        ...removeNilComponents(
          Heading(title),
          Subtitle(time),
          ...TextParagraphs(question),
          ...TextParagraphs(shortDescription),
          ...TextParagraphs(longDescription),
        ),
      ),
      ButtonGroup(...buttons),
    ),
  );

export default writing;
