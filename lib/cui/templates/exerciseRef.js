import {
  Section,
  ButtonGroup,
  Heading,
  TextGroup,
  MiniBadge,
  TextParagraphs,
  CollapsibleList,
  VideoStories,
} from '../components/index.js';
import { removeNilComponents } from '../utils.js';

export default ({ sectionId }, { refId, ...section }, exercises = {}) => {
  const exercise = exercises[refId];
  if (!exercise) {
    return Section(sectionId)(
      removeNilComponents(
        TextGroup(Heading('Exercise not found')),
        ButtonGroup(...section.buttons),
      ),
    );
  }

  return Section(sectionId)(
    removeNilComponents(
      TextGroup(
        ...removeNilComponents(
          MiniBadge(
            'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/exercise_badge_ltbfuq.png',
          ),
          Heading(exercise.name),
          ...TextParagraphs(exercise.description),
          CollapsibleList(exercise.items),
        ),
      ),
      VideoStories(exercise.VideoStories && exercise.VideoStories.content),
      ButtonGroup(...section.buttons),
    ),
  );
};
