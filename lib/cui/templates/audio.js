import {
  TextGroup,
  Heading,
  TextParagraphs,
  ButtonGroup,
  AudioPlayer,
  Section,
  Image,
} from '../components/index.js';

import { removeNilComponents } from '../utils.js';

const audio = (
  { sectionId },
  { imageUri, title, description, audioUri, duration, buttons = [] },
) =>
  Section(sectionId)(
    removeNilComponents(
      Image(imageUri),
      TextGroup(
        ...removeNilComponents(Heading(title), ...TextParagraphs(description)),
      ),
      AudioPlayer(audioUri, duration),
      ButtonGroup(...buttons),
    ),
  );

export default audio;
