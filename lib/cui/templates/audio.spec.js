import audio from './audio.js';

describe('audio', () => {
  it('returns an object', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
    };
    const data = {
      title: 'A title',
      description: 'The description',
      audioUri: 'gopher://audio:1337',
      buttons: [
        {
          component: 'Button',
          content: 'Go go go',
          conversationId: 'some-conversation-id',
          sectionId: 'u-turn',
        },
      ],
    };

    expect(audio(parent, data)).toEqual({
      id: 'some-section-id',
      content: [
        {
          component: 'TextGroup',
          content: [
            { component: 'Text', content: '# A title' },
            { component: 'Text', content: 'The description' },
          ],
        },
        {
          component: 'AudioPlayer',
          content: { duration: undefined, uri: 'gopher://audio:1337' },
        },
        {
          component: 'ButtonGroup',
          content: [
            {
              component: 'Button',
              content: 'Go go go',
              conversationId: 'some-conversation-id',
              sectionId: 'u-turn',
            },
          ],
        },
      ],
    });
  });
});
