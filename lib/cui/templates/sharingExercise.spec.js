import sharingExerciseTemplate from './sharingExercise.js';

jest.mock('../../../src/ui/CUI.SharingExercise.json', () => ({
  en: {
    sharingWall: {
      title: 'Reflection exercise',
      body__markdown: 'Here is what others have said',
      skip__button: 'Skip',
    },
    exerciseInput: {
      title: 'Reflection exercise',
      input_placeholder: 'Write your reflection here...',
      skip__button: 'No, not right now',
    },
    exerciseSharingChoice: {
      title: 'Your reflection',
      body__markdown:
        'Would you like to share this reflection?\nAnonymously, of course.',
      yes__button: 'Sure!',
      no__button: 'I’d rather not',
    },
    exerciseShared: {
      heading__markdown: '## Thanks for ~~sharing~~',
      body__markdown:
        'Sharing is another important component of personal growth.',
      continue__button: 'Continue',
    },
    exerciseNotShared: {
      heading__markdown: "## It's okay",
      body__markdown:
        'You have chosen not to share your reflections and that it is perfectly okay. Take it at your own pace. You will have several opportunities.',
      continue__button: 'Okay',
    },
    exerciseSkipped: {
      body__markdown: 'Maybe next time, here is what others have said.',
      continue__button: 'Continue',
    },
  },
}));

describe('sharingExerciseTemplate', () => {
  it('returns an object', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
      lang: 'en',
    };
    const data = {
      question: 'Some question',
      exampleText: 'Some example text',
      nextConversation: 'some-next-conversation-id',
      nextSection: 'some-next-section-id',
    };

    expect(sharingExerciseTemplate(parent, data)).toEqual([
      {
        id: 'some-section-id',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Subtitle',
                content: 'Reflection exercise',
              },
              {
                component: 'Text',
                content: '## Some question',
              },
            ],
          },
          {
            component: 'Input',
            props: {
              target: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-sharingChoice',
              },
              enableSkip: true,
              skipText: 'Skip',
              skipTarget: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didSkipShare',
              },
              placeholder: 'Write your reflection here...',
              questionKey:
                'some-conversation-id-some-section-id-sharingExercise',
            },
          },
        ],
      },
      {
        id: 'some-section-id-sharingChoice',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Here is what others have said',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'some-conversation-id-some-section-id',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content:
                  'Would you like to share this reflection?\nAnonymously, of course.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'SubmitSharingButton',
                content: 'Sure!',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didShare',
                props: {
                  key: 'some-conversation-id-some-section-id-sharingExercise',
                  category: 'some-conversation-id-some-section-id',
                  question: 'Some question',
                },
              },
              {
                component: 'Button',
                content: 'I’d rather not',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didNotShare',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: '## Thanks for ~~sharing~~',
              },
              {
                component: 'Text',
                content:
                  'Sharing is another important component of personal growth.',
              },
            ],
          },
          {
            component: 'ExerciseReview',
            props: {
              key: 'some-conversation-id-some-section-id-sharingExercise',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Continue',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didNotShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: "## It's okay",
              },
              {
                component: 'Text',
                content:
                  'You have chosen not to share your reflections and that it is perfectly okay. Take it at your own pace. You will have several opportunities.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Okay',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didSkipShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Maybe next time, here is what others have said.',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'some-conversation-id-some-section-id',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Continue',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
    ]);
  });

  it('returns an object with custom categoryKey', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
      lang: 'en',
    };
    const data = {
      categoryKey: 'new-category-key',
      question: 'Some question',
      exampleText: 'Some example text',
      buttonText: 'I am done',
      nextConversation: 'some-next-conversation-id',
      nextSection: 'some-next-section-id',
    };

    expect(sharingExerciseTemplate(parent, data)).toEqual([
      {
        id: 'some-section-id',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Subtitle',
                content: 'Reflection exercise',
              },
              {
                component: 'Text',
                content: '## Some question',
              },
            ],
          },
          {
            component: 'Input',
            props: {
              target: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-sharingChoice',
              },
              enableSkip: true,
              skipText: 'Skip',
              skipTarget: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didSkipShare',
              },
              placeholder: 'Write your reflection here...',
              questionKey:
                'some-conversation-id-some-section-id-sharingExercise',
            },
          },
        ],
      },
      {
        id: 'some-section-id-sharingChoice',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Here is what others have said',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content:
                  'Would you like to share this reflection?\nAnonymously, of course.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'SubmitSharingButton',
                content: 'Sure!',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didShare',
                props: {
                  key: 'some-conversation-id-some-section-id-sharingExercise',
                  category: 'new-category-key',
                  question: 'Some question',
                },
              },
              {
                component: 'Button',
                content: 'I’d rather not',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didNotShare',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: '## Thanks for ~~sharing~~',
              },
              {
                component: 'Text',
                content:
                  'Sharing is another important component of personal growth.',
              },
            ],
          },
          {
            component: 'ExerciseReview',
            props: {
              key: 'some-conversation-id-some-section-id-sharingExercise',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didNotShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: "## It's okay",
              },
              {
                component: 'Text',
                content:
                  'You have chosen not to share your reflections and that it is perfectly okay. Take it at your own pace. You will have several opportunities.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didSkipShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Maybe next time, here is what others have said.',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
                sectionId: 'some-next-section-id',
              },
            ],
          },
        ],
      },
    ]);
  });

  it('defaults to root section if nextConversation is provided', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
      lang: 'en',
    };
    const data = {
      categoryKey: 'new-category-key',
      question: 'Some question',
      exampleText: 'Some example text',
      buttonText: 'I am done',
      nextConversation: 'some-next-conversation-id',
    };

    expect(sharingExerciseTemplate(parent, data)).toEqual([
      {
        id: 'some-section-id',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Subtitle',
                content: 'Reflection exercise',
              },
              {
                component: 'Text',
                content: '## Some question',
              },
            ],
          },
          {
            component: 'Input',
            props: {
              target: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-sharingChoice',
              },
              enableSkip: true,
              skipText: 'Skip',
              skipTarget: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didSkipShare',
              },
              placeholder: 'Write your reflection here...',
              questionKey:
                'some-conversation-id-some-section-id-sharingExercise',
            },
          },
        ],
      },
      {
        id: 'some-section-id-sharingChoice',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Here is what others have said',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content:
                  'Would you like to share this reflection?\nAnonymously, of course.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'SubmitSharingButton',
                content: 'Sure!',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didShare',
                props: {
                  key: 'some-conversation-id-some-section-id-sharingExercise',
                  category: 'new-category-key',
                  question: 'Some question',
                },
              },
              {
                component: 'Button',
                content: 'I’d rather not',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didNotShare',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: '## Thanks for ~~sharing~~',
              },
              {
                component: 'Text',
                content:
                  'Sharing is another important component of personal growth.',
              },
            ],
          },
          {
            component: 'ExerciseReview',
            props: {
              key: 'some-conversation-id-some-section-id-sharingExercise',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didNotShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: "## It's okay",
              },
              {
                component: 'Text',
                content:
                  'You have chosen not to share your reflections and that it is perfectly okay. Take it at your own pace. You will have several opportunities.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didSkipShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Maybe next time, here is what others have said.',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'I am done',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
    ]);
  });

  it('removes early skip button', () => {
    const parent = {
      conversationId: 'some-conversation-id',
      sectionId: 'some-section-id',
      lang: 'en',
    };
    const data = {
      categoryKey: 'new-category-key',
      question: 'Some question',
      exampleText: 'Some example text',
      nextConversation: 'some-next-conversation-id',
      enableEarlySkip: false,
    };

    expect(sharingExerciseTemplate(parent, data)).toEqual([
      {
        id: 'some-section-id',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Subtitle',
                content: 'Reflection exercise',
              },
              {
                component: 'Text',
                content: '## Some question',
              },
            ],
          },
          {
            component: 'Input',
            props: {
              target: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-sharingChoice',
              },
              enableSkip: false,
              skipText: 'Skip',
              skipTarget: {
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didSkipShare',
              },
              placeholder: 'Write your reflection here...',
              questionKey:
                'some-conversation-id-some-section-id-sharingExercise',
            },
          },
        ],
      },
      {
        id: 'some-section-id-sharingChoice',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Here is what others have said',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content:
                  'Would you like to share this reflection?\nAnonymously, of course.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'SubmitSharingButton',
                content: 'Sure!',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didShare',
                props: {
                  key: 'some-conversation-id-some-section-id-sharingExercise',
                  category: 'new-category-key',
                  question: 'Some question',
                },
              },
              {
                component: 'Button',
                content: 'I’d rather not',
                conversationId: 'some-conversation-id',
                sectionId: 'some-section-id-didNotShare',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: '## Thanks for ~~sharing~~',
              },
              {
                component: 'Text',
                content:
                  'Sharing is another important component of personal growth.',
              },
            ],
          },
          {
            component: 'ExerciseReview',
            props: {
              key: 'some-conversation-id-some-section-id-sharingExercise',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Continue',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didNotShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: "## It's okay",
              },
              {
                component: 'Text',
                content:
                  'You have chosen not to share your reflections and that it is perfectly okay. Take it at your own pace. You will have several opportunities.',
              },
            ],
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Okay',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
      {
        id: 'some-section-id-didSkipShare',
        content: [
          {
            component: 'TextGroup',
            content: [
              {
                component: 'Text',
                content: 'Maybe next time, here is what others have said.',
              },
            ],
          },
          {
            component: 'ExerciseCarousel',
            props: {
              category: 'new-category-key',
              exampleText: 'Some example text',
            },
          },
          {
            component: 'ButtonGroup',
            content: [
              {
                component: 'Button',
                content: 'Continue',
                conversationId: 'some-next-conversation-id',
              },
            ],
          },
        ],
      },
    ]);
  });
});
