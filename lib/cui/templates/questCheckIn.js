import createRequire from '../../../lib/require.cjs';
import {
  TextGroup,
  Text,
  Guiding,
  ButtonGroup,
  Button,
  Badge,
  MiniBadge,
  Section,
  FillViewport,
} from '../components/index.js';

const cjsRequire = createRequire(import.meta.url);
const translations = cjsRequire('../../../src/ui/CUI.QuestCheckIn.json');

const questionIDs = {
  Q1: 'q1',
  Q2: 'q2',
  Q3: 'q3',
  Q4: 'q4',
  Q5: 'q5',
  Q6: 'q6',
  Q7: 'q7',
  Q8: 'q8',
};
const sectionIDs = {
  FOLLOW_UP: 'followUp',
  ...questionIDs,
  FOLLOW_UP_COMPLETE: 'followUpComplete',
};

const template = (parent, data) => {
  const { conversationId, lang } = parent;
  const {
    questCompleteSectionId,
    skipFollowUpSectionId,
    taskCompletedSectionId,
    initialDay,
    task,
    autoProgress,
  } = data;
  const t = translations[lang];
  const shortOptions = Object.values(t.followUp3).map((label, index) => ({
    label,
    score: index,
  }));
  const longOptions = Object.values(t.followUp7).map((label, index) => ({
    label,
    score: index,
  }));

  const questions = Object.values(questionIDs).map(
    (sectionId) =>
      (initialDay
        ? t[sectionId].initialDayQuestion__markdown
        : t[sectionId].question__markdown) || t[sectionId].question__markdown,
  );

  const groupedQuestions = [
    {
      questions: questions.slice(0, 3),
      options: shortOptions,
    },
    {
      questions: questions.slice(3),
      options: longOptions,
    },
  ];

  return [
    Section(
      sectionIDs.FOLLOW_UP,
      null,
      FillViewport(),
    )(
      [
        Badge(
          'https://res.cloudinary.com/twentyninek/image/upload/v1617023623/Badges/check-in_badge_tlspms.png',
        ),
        TextGroup(
          Text(t[sectionIDs.FOLLOW_UP].title__markdown),
          MiniBadge(
            'https://res.cloudinary.com/twentyninek/image/upload/v1617025399/Badges/quest_mini-badge_qgumki.png',
          ),
          Text(t[sectionIDs.FOLLOW_UP].info__markdown),
          Guiding(task.body),
          Text(t[sectionIDs.FOLLOW_UP].question),
        ),
        ButtonGroup(
          Button(
            t[sectionIDs.FOLLOW_UP].taskCompleted__button,
            conversationId,
            taskCompletedSectionId,
          ),
          Button(
            t[sectionIDs.FOLLOW_UP].taskNotCompleted__button,
            conversationId,
            skipFollowUpSectionId,
          ),
        ),
      ],
      autoProgress,
    ),

    Section(
      sectionIDs.Q1,
      null,
      FillViewport(),
    )([
      TextGroup(
        Text(t[sectionIDs.Q1].paragraph1__markdown),
        Text(t[sectionIDs.Q1].paragraph2__markdown),
      ),
      ButtonGroup(
        Button(t.followUp.startTest__button, conversationId, sectionIDs.Q2),
      ),
    ]),

    Section(sectionIDs.Q2)([
      {
        next: {
          sectionId: sectionIDs.FOLLOW_UP_COMPLETE,
        },
        component: 'QuestionnaireSet',
        sets: groupedQuestions,
      },
    ]),

    Section(sectionIDs.FOLLOW_UP_COMPLETE)([
      TextGroup(
        Text(t.followUpComplete.title__markdown),
        Text(t.followUpComplete.paragraph1__markdown),
        Text(t.followUpComplete.paragraph2__markdown),
      ),
      ButtonGroup(
        Button(
          t.followUpComplete.complete__button,
          conversationId,
          questCompleteSectionId,
        ),
      ),
    ]),
  ];
};

export { template, sectionIDs };
