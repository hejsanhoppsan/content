/*
 * Copyright (c) 2018-2021 29k International AB
 */

import { compose, join, replace } from 'ramda';
import { v5 as uuidv5 } from 'uuid';
import bs58 from 'bs58';

// encode :: [Byte] -> String
const encode = bs58.encode;
// decode :: String -> [Byte]
const decode = bs58.decode;

// namespace for 29k (v4)
const namespace = '2d2d2d3c-7b7b-4020-82bf-2040292d2d7d';

const uuidv5Bytes = (name) => uuidv5(name, namespace, new Array());
const createId = compose(encode, uuidv5Bytes);

const uuidToHex = replace(/-/g, '');
const hexToBytes = (hex) => Buffer.from(hex, 'hex');
const uuidToBytes = compose(hexToBytes, uuidToHex);

const bytesToUuid = (bytes) =>
  join('-', [
    bytes.subarray(0, 4).toString('hex'),
    bytes.subarray(4, 6).toString('hex'),
    bytes.subarray(6, 8).toString('hex'),
    bytes.subarray(8, 10).toString('hex'),
    bytes.subarray(10).toString('hex'),
  ]);

const uuidToId = compose(encode, uuidToBytes);
const idToUuid = compose(bytesToUuid, decode);

const dateString = (date) => Math.floor(date.getTime() / 1e3).toString();

const createPendingLessonUri = (lessonId, userId) =>
  join(':', ['lessonId', lessonId, 'user', userId]);

const createLessonScheduleId = (lessonId, userId) =>
  createId(createPendingLessonUri(lessonId, userId));

// createCourseChannelId creates an id for a channel given a course id.
const createCourseChannelId = (courseId) =>
  createId('channel:course:' + courseId);

const createGroupSharingUri = (groupId, lessonId, sharingAt) =>
  join(':', [
    'group',
    groupId,
    'lesson',
    lessonId,
    'sharing',
    dateString(sharingAt),
  ]);

// createGroupSharingId creates an id for a group sharing given lesson and date.
const createGroupSharingId = (groupId, lessonId, sharingAt) =>
  createId(createGroupSharingUri(groupId, lessonId, sharingAt));

// createRtcRoomId creates an id for a real-time communications room for a
// group.
const createRtcRoomId = (groupId) => createId(`group:${groupId}:video`);

// createRtcUserId creates an id for a user in a real-time communications room.
const createRtcUserId = (roomId, userId) =>
  createId(`room:${roomId}:user:${userId}`);

// createConversationId creates an id based on courseId + lessonId or exerciseId
const createConversationIdFromLesson = (courseId, lessonId) =>
  createId(`course:${courseId}:lesson:${lessonId}`);

export {
  encode,
  decode,
  uuidToId,
  idToUuid,
  namespace,
  dateString, // only exported for test,
  createLessonScheduleId,
  createConversationIdFromLesson,
  createCourseChannelId,
  createGroupSharingId,
  createRtcRoomId,
  createRtcUserId,
};
