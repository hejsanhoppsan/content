import {
  groupBy,
  prop,
  pluck,
  mapObjIndexed,
  chain,
  pipe,
  flatten,
  values,
  mergeRight,
  toPairs,
  map,
  head,
  pick,
  applySpec,
} from 'ramda';

const groupByContentType = pipe(
  groupBy(prop('type')),
  mapObjIndexed(pluck('data')),
);

const translatedFields = ['CONVERSATIONS', 'COURSES', 'EXERCISES'];

const withLangAndContentType = (value, key) =>
  chain(
    pipe(
      toPairs,
      map(([lang, data]) => ({ lang, data, type: key })),
    ),
  )(value);

const denormalize = pipe(
  mapObjIndexed(withLangAndContentType),
  values,
  flatten,
);

const indexById = pipe(groupBy(prop('id')), mapObjIndexed(head));
const indexSections = (conversations = []) =>
  conversations.map((conversation) =>
    conversation.sections
      ? { ...conversation, sections: indexById(conversation.sections) }
      : conversation,
  );

// Transforms content into a form that's more
// easily traversed and more amenable to testing.
//
// Before:
// {
//   CONVERSATIONS: [{ en: { id: hehu }, sv: { id: hehu } }] ,
//   COURSES: [{en: 'course'}, {sv: 'course'}],
//   other: [...]
// }
//
// After:
// [
//   ['en', {CONVERSATIONS: [...], COURSES: [...], other: [...]}],
//   ['sv', {CONVERSATIONS: [...], COURSES: [...], other: [...]}]
// ]
const groupContentByLang = (data) =>
  pipe(
    pick(translatedFields),
    denormalize,
    groupBy(prop('lang')),
    mapObjIndexed(groupByContentType),
    mapObjIndexed(mergeRight(data)),
    toPairs,
  )(data);

// Indexed content by ID, a form that is more convenient for
// look-ups. E.g., path([conversationId, sectionId])
//
// Example:
// [
//   {
//     id: 'hehu',
//     sections: [{ id: 'sectionId' }]
//   }
// ]
//
// =>
//
// {
//   hehu: {
//     id: 'hehu',
//     sections: {sectionId: { id: 'sectionId' }}
//   }
// }
//
// TODO: Lessons within courses are not yet indexed
const indexContent = applySpec({
  CONVERSATIONS: pipe(prop('CONVERSATIONS'), indexSections, indexById),
  COURSES: pipe(prop('COURSES'), indexById),
  EXERCISES: pipe(prop('EXERCISES'), indexById),
});

export { groupContentByLang, indexContent };
