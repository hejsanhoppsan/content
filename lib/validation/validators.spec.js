import {
  createRootValidator,
  courseValidator,
  lessonValidator,
  conversationValidator,
  sectionValidator,
  componentValidator,
  exerciseValidator,
  buttonGroupChildValidator,
  conversationConnectionValidator,
} from './validators.js';

import {
  testData as content,
  indexedContent,
  contentByLang,
} from './testData.js';

describe('rootValidator', () => {
  it('should throw error if 1 more errors were reported', () => {
    const reporter = {
      errors: [],
      error: () => reporter.errors.push(new Error('lol')),
    };

    const validator = createRootValidator(reporter)([
      (content, data) => data.reporter.error(),
    ]);

    expect(() => {
      validator({ CONVERSATIONS: [{ en: { id: 'hellu' } }] });
    }).toThrow();
  });

  it('Validators should correctly pass the appropriate data to each rule', () => {
    const courseRule = jest.fn();
    const lessonRule = jest.fn();
    const exerciseRule = jest.fn();
    const conversationRule = jest.fn();
    const sectionRule = jest.fn();
    const componentRule = jest.fn();
    const buttonRule = jest.fn();

    const reporter = { error: () => {}, errors: [] };

    const rootValidator = createRootValidator(reporter);

    const dataEn = {
      lang: 'en',
      reporter,
      content: contentByLang['en'],
      indexedContent: indexedContent['en'],
      path: [],
    };
    const dataSv = {
      lang: 'sv',
      reporter,
      content: contentByLang['sv'],
      indexedContent: indexedContent['sv'],
      path: [],
    };

    rootValidator([
      courseValidator([courseRule, lessonValidator([lessonRule])]),
      exerciseValidator([exerciseRule]),
      conversationValidator([
        conversationRule,
        sectionValidator([
          sectionRule,
          componentValidator([
            componentRule,
            buttonGroupChildValidator([buttonRule]),
          ]),
        ]),
      ]),
    ])(content);

    expect(courseRule).toHaveBeenCalledTimes(2);
    expect(courseRule).toHaveBeenNthCalledWith(
      1,
      { id: 'course-en', lessons: [{ id: 'lesson-en' }], wip: false },
      { ...dataEn, path: [contentByLang['en']] },
    );
    expect(courseRule).toHaveBeenNthCalledWith(
      2,
      { id: 'course-sv', lessons: [{ id: 'lesson-sv' }], wip: false },
      { ...dataSv, path: [contentByLang['sv']] },
    );

    expect(lessonRule).toHaveBeenCalledTimes(2);
    expect(lessonRule).toHaveBeenNthCalledWith(
      1,
      { id: 'lesson-en' },
      {
        ...dataEn,
        path: [contentByLang['en'].COURSES[0], contentByLang['en']],
      },
    );
    expect(lessonRule).toHaveBeenNthCalledWith(
      2,
      { id: 'lesson-sv' },
      {
        ...dataSv,
        path: [contentByLang['sv'].COURSES[0], contentByLang['sv']],
      },
    );

    expect(conversationRule).toHaveBeenCalledTimes(2);
    expect(conversationRule).toHaveBeenNthCalledWith(
      1,
      {
        id: 'conversation-en',
        sections: [
          {
            id: 'section',
            content: [
              { component: 'SomeComponent' },
              { component: 'SomeOtherComponent' },
              { component: 'ButtonGroup', content: [{ component: 'Button' }] },
            ],
          },
        ],
      },
      { ...dataEn, path: [contentByLang['en']] },
    );
    expect(conversationRule).toHaveBeenNthCalledWith(
      2,
      {
        id: 'conversation-sv',
      },
      { ...dataSv, path: [contentByLang['sv']] },
    );

    expect(exerciseRule).toHaveBeenCalledTimes(2);
    expect(exerciseRule).toHaveBeenNthCalledWith(
      1,
      { id: 'exercise-en' },
      { ...dataEn, path: [contentByLang['en']] },
    );
    expect(exerciseRule).toHaveBeenNthCalledWith(
      2,
      { id: 'exercise-sv' },
      { ...dataSv, path: [contentByLang['sv']] },
    );

    expect(sectionRule).toHaveBeenCalledTimes(1);
    expect(sectionRule).toHaveBeenNthCalledWith(
      1,
      {
        id: 'section',
        content: [
          { component: 'SomeComponent' },
          { component: 'SomeOtherComponent' },
          { component: 'ButtonGroup', content: [{ component: 'Button' }] },
        ],
      },
      {
        ...dataEn,
        path: [contentByLang['en'].CONVERSATIONS[0], contentByLang['en']],
      },
    );

    expect(componentRule).toHaveBeenCalledTimes(3);
    expect(componentRule).toHaveBeenNthCalledWith(
      1,
      { component: 'SomeComponent' },
      {
        ...dataEn,
        path: [
          contentByLang['en'].CONVERSATIONS[0].sections[0],
          contentByLang['en'].CONVERSATIONS[0],
          contentByLang['en'],
        ],
      },
    );
    expect(componentRule).toHaveBeenNthCalledWith(
      2,
      { component: 'SomeOtherComponent' },
      {
        ...dataEn,
        path: [
          contentByLang['en'].CONVERSATIONS[0].sections[0],
          contentByLang['en'].CONVERSATIONS[0],
          contentByLang['en'],
        ],
      },
    );

    expect(componentRule).toHaveBeenNthCalledWith(
      3,
      { component: 'ButtonGroup', content: [{ component: 'Button' }] },
      {
        ...dataEn,
        path: [
          contentByLang['en'].CONVERSATIONS[0].sections[0],
          contentByLang['en'].CONVERSATIONS[0],
          contentByLang['en'],
        ],
      },
    );

    expect(buttonRule).toHaveBeenCalledTimes(1);
    expect(buttonRule).toHaveBeenNthCalledWith(
      1,
      { component: 'Button' },
      {
        ...dataEn,
        path: [
          contentByLang['en'].CONVERSATIONS[0].sections[0].content[2],
          contentByLang['en'].CONVERSATIONS[0].sections[0],
          contentByLang['en'].CONVERSATIONS[0],
          contentByLang['en'],
        ],
      },
    );
  });
});

describe('buttonGroupValidator', () => {
  it('should call rules with each button inside a button group', () => {
    const rule = jest.fn();
    const buttonGroup = {
      component: 'ButtonGroup',
      content: [{ component: 'Button1' }, { component: 'Button2' }],
    };

    buttonGroupChildValidator([rule])(buttonGroup, {});

    expect(rule).toHaveBeenCalledTimes(2);
    expect(rule).toHaveBeenCalledWith(
      { component: 'Button1' },
      {
        path: [buttonGroup],
      },
    );
  });

  it('should ignore non-ButtonGroup components', () => {
    const rule = jest.fn();
    const validate = buttonGroupChildValidator([rule]);

    [
      ({ component: 'TextGroup' },
      { component: 'namWasHere' },
      { component: 'Button' }),
    ].forEach((component) => validate(component, {}));

    expect(rule).not.toHaveBeenCalled();
  });
});

describe('conversationConnectionValidator', () => {
  it('should pass the concrete conversations to each rule', () => {
    const lessonWithConversations = {
      conversations: [
        { conversationId: 'conversationRef1' },
        { conversationId: 'conversationRef2' },
      ],
    };

    const contextData = {
      content: {
        CONVERSATIONS: [
          { id: 'conversationRef1', sections: [] },
          { id: 'conversationRef2', sections: [] },
        ],
      },
    };
    const rule1 = jest.fn();

    conversationConnectionValidator([rule1])(
      lessonWithConversations,
      contextData,
    );

    expect(rule1).toHaveBeenCalledTimes(2);
    expect(rule1).toHaveBeenNthCalledWith(
      1,
      { id: 'conversationRef1', sections: [] },
      { ...contextData, path: [lessonWithConversations] },
    );
    expect(rule1).toHaveBeenNthCalledWith(
      2,
      { id: 'conversationRef2', sections: [] },
      { ...contextData, path: [lessonWithConversations] },
    );
  });

  it('should ignore invalid conversation references', () => {
    const lessonWithConversations = {
      conversations: [{ conversationId: 'foo' }, { conversationId: 'bar' }],
    };

    const contextData = {
      content: {
        CONVERSATIONS: [
          { id: 'conversationRef1', sections: [] },
          { id: 'conversationRef2', sections: [] },
        ],
      },
    };
    const rule1 = jest.fn();

    conversationConnectionValidator([rule1])(
      lessonWithConversations,
      contextData,
    );

    expect(rule1).not.toHaveBeenCalled();
  });
});

describe('lessonValidator', () => {
  it('should ignore lesson if parent course is WIP', () => {
    const courseWithLessons = {
      wip: true,
      lessons: [{}],
    };

    const rule1 = jest.fn();

    lessonValidator([rule1])(courseWithLessons, {});

    expect(rule1).not.toHaveBeenCalled();
  });

  it('should validate lesson if parent course is not WIP', () => {
    const courseWithLessons = {
      wip: false,
      lessons: [{}, {}],
    };

    const rule1 = jest.fn();

    lessonValidator([rule1])(courseWithLessons, {});

    expect(rule1).toHaveBeenCalledTimes(2);
  });
});
