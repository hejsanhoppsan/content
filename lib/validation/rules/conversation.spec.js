import { rootSectionExists } from './conversation.js';

describe('rootSectionExists', () => {
  it('should reject root IDs referring to non-existing section IDs', () => {
    const testData = {
      id: 'gh1fZa-asdzf-aslewl',
      alias: 'Some conversation',
      root: 'ghost-section',
      sections: [],
    };
    expect(() => {
      rootSectionExists(testData);
    }).toThrow(
      new Error(
        'Root section (ghost-section) does not exist in conversation gh1fZa-asdzf-aslewl (alias: Some conversation)',
      ),
    );
  });

  it('should accept root IDs referring to existing sections', () => {
    const testData = {
      id: 'gh1fZa-asdzf-aslewl',
      alias: 'Some conversation',
      root: 'ghost-section',
      sections: [
        {
          id: 'ghost-section',
        },
      ],
    };
    expect(() => {
      rootSectionExists(testData);
    }).not.toThrow();
  });

  it('should ignore conversation stubs', () => {
    const testData = {
      id: 'gh1fZa-asdzf-aslewl',
      alias: 'Some conversation',
    };
    expect(() => {
      rootSectionExists(testData);
    }).not.toThrow();
  });
});
