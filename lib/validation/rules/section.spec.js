import { ContentValidationError } from '../errors.js';
import { noStandaloneTexts } from './section.js';

describe('noStandaloneTexts', () => {
  it('should throw if Text Components are found outside a Text Group', () => {
    const section = {
      content: [{ component: 'Text' }],
    };
    expect(() => {
      noStandaloneTexts(section);
    }).toThrow(ContentValidationError);
  }),
    it('should accept TextGroups', () => {
      const section = {
        content: [{ component: 'TextGroup' }],
      };
      expect(() => {
        noStandaloneTexts(section);
      }).not.toThrow();
    });
});
