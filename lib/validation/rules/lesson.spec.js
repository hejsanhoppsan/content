import { ContentValidationError } from '../errors.js';
import {
  noInvalidConversationRef,
  noExternalConversationRefs,
} from './lesson.js';

describe('noInvalidConversationRef', () => {
  const data = {
    indexedContent: {
      CONVERSATIONS: {
        iDoExist: {},
      },
    },
    path: [{ id: 'someCourse' }],
  };

  it('should reject lessons with invalid conversation references', () => {
    const lesson = {
      conversations: [{ conversationId: 'iDoNotExist' }],
    };

    expect(() => noInvalidConversationRef(lesson, data)).toThrow(
      ContentValidationError,
    );
  });

  it('should accept lessons without conversation references', () => {
    expect(() => noInvalidConversationRef({}, data)).not.toThrow();
  });

  it('should accept lessons with valid conversation references', () => {
    const lesson = { conversations: [{ conversationId: 'iDoExist' }] };
    expect(() => noInvalidConversationRef(lesson, data)).not.toThrow();
  });
});

describe('noExternalConversationRefs', () => {
  it('should accept conversations that reference only conversations that have been declared by the lesson', () => {
    const button = {
      component: 'Button',
      conversationId: 'some-conversation',
    };
    const section = { id: 'some-section' };
    const conversationConnection = {
      id: 'some-conversation',
      sections: [section],
    };
    const lesson = {
      id: 'some-lesson',
      conversations: [{ conversationId: 'some-conversation' }],
    };
    const course = {
      id: 'some-course',
      lessons: [lesson],
    };

    const data = {
      path: [
        null, // ButtonGroup,
        section,
        conversationConnection,
        lesson,
        course,
      ],
    };

    expect(() => {
      noExternalConversationRefs(button, data);
    }).not.toThrow(ContentValidationError);
  });

  it('should reject conversations (declared by a lesson) that reference undeclared conversations', () => {
    const button = {
      component: 'Button',
      conversationId: 'some-external-conversation',
    };
    const section = { id: 'some-section' };
    const conversationConnection = {
      id: 'some-conversation',
      sections: [section],
    };
    const lesson = {
      id: 'some-lesson',
      conversations: [{ conversationId: 'some-conversation' }],
    };
    const course = {
      id: 'some-course',
      lessons: [lesson],
    };

    const data = {
      path: [
        null, // ButtonGroup,
        section,
        conversationConnection,
        lesson,
        course,
      ],
    };

    expect(() => {
      noExternalConversationRefs(button, data);
    }).toThrow(ContentValidationError);
  });
});
