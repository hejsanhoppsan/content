import { path, propOr, has } from 'ramda';
import { ContentValidationError } from '../errors.js';

const validButtonTypes = [
  'Button',
  'CloseButton',
  'SegmentButton',
  'LessonCompleteButton',
  'SubmitSharingButton',
  'CompleteLessonButton',
  'NextConversationButton',
  'ExerciseCompleteButton',
  'CheckPNsStatusButton',
  'RequestPNsButton',
  'TestCompleteButton',
];

const validTextGroupChildren = [
  'Text',
  'Subtitle',
  'SmallPrint',
  'Quote',

  // TODO: Many TextGroup components currently contain these
  // elements and so are included in this list. But are they
  // really valid 'Text' components?
  'MiniBadge',
  'HeaderImage',
  'Guiding',
  'CollapsibleList',
];

const getConversationById = (id, data) =>
  path(['indexedContent', 'CONVERSATIONS', id], data);

const sectionNotFoundError = (sectionId, conversationId, alias) =>
  new ContentValidationError(
    `Section '${sectionId}' does not exist in conversation ${conversationId} (alias: ${alias})`,
  );

const noEmptyButtonGroups = (element) => {
  if (element.component !== 'ButtonGroup') return;
  if (!element.content || element.content.length < 1) {
    throw new ContentValidationError('Empty button group');
  }
};

const noButtonsWithoutAGroup = (element, data) => {
  const [section, conversation] = propOr([], 'path', data);
  if (validButtonTypes.includes(element.component)) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: A Button should have an enclosing ButtonGroup`,
    );
  }
};

const containsOnlyValidVideoRefs = (element, data) => {
  const [section, conversation] = propOr([], 'path', data);

  if (element.component !== 'VideoStories') return;

  if (element.content.length === 0) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: VideoStories must contain at least one video`,
    );
  }

  const videos = propOr([], 'content', element);
  videos.forEach((video) => {
    if (!has('videoUri', video) || !has('imageUri', video)) {
      throw new ContentValidationError(
        `[${conversation.id} / ${section.id}]: Every video must contain a videoUri and an imageUri`,
      );
    }
  });
};

const containsOnlyValidButtons = (element, data) => {
  const [section, conversation] = propOr([], 'path', data);

  if (element.component !== 'ButtonGroup') return;
  const buttons = propOr([], 'content', element);
  const errors = [];
  buttons.forEach((button) => {
    if (!validButtonTypes.includes(button.component))
      errors.push(`Component: ${button.component}`);
  });

  if (errors.length > 0) {
    throw new ContentValidationError(
      `[${conversation.id} / ${
        section.id
      }]: Button group contains invalid button types:\n  ${errors.join(
        '\n  ',
      )}`,
    );
  }
};

const buttonsMustHaveTargetRef = (component, data) => {
  const [, section, conversation] = propOr([], 'path', data);
  if (
    ['Button', 'NextConversationButton'].includes(component.component) &&
    !component.conversationId &&
    !component.sectionId
  ) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]:\n  Buttons must have at least one of 'sectionId' and 'conversationId'`,
    );
  }
};

const noInvalidReferences = (button, data) => {
  const [, section, conversation] = propOr([], 'path', data);
  const { conversationId: cid, sectionId } = button;
  const conversationId = cid ? cid : conversation.id;
  const targetConversation = getConversationById(conversationId, data);
  if (!targetConversation) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: Conversation '${conversationId}' does not exist.`,
    );
  }

  // targetSection was not provided, but target
  // conversation was found. Assume the target section is
  // equal to the value of the conversation's root
  // property.
  if (!sectionId) return;

  const targetSectionId =
    sectionId === 'root' ? targetConversation.root : sectionId;

  const targetSection = path(['sections', targetSectionId], targetConversation);

  if (!targetSection && sectionId !== 'END') {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: ${
        sectionNotFoundError(
          targetSectionId,
          conversationId,
          targetConversation.alias,
        ).message
      }`,
    );
  }
};

const validNextConversationTarget = (button, data) => {
  const [, section, conversation] = propOr([], 'path', data);
  const { conversationId, component } = button;

  if (
    (!conversationId || conversation.id === conversationId) &&
    component === 'NextConversationButton'
  ) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: NextConversationButton should target a different conversation id`,
    );
  }
};

const containsOnlyValidTextComponents = (component, data) => {
  if (component.component !== 'TextGroup') return;

  const [, section, conversation] = propOr([], 'path', data);

  component.content.forEach((child) => {
    if (!validTextGroupChildren.includes(child.component)) {
      throw new ContentValidationError(
        `[${conversation.id} / ${section.id}]: TextGroup contains invalid component: ${child.component}`,
      );
    }
  });
};

const hasProps = (targetComponent, props) => (component, data) => {
  if (component.component !== targetComponent) return;

  const missingProps = [];
  const [, section, conversation] = propOr([], 'path', data);

  props.forEach((prop) => {
    if (!has(prop, component)) {
      missingProps.push(prop);
    }
  });

  if (missingProps.length > 0) {
    throw new ContentValidationError(
      `[${conversation.id} / ${section.id}]: Component ${targetComponent} is missing the following props: ${missingProps}`,
    );
  }
};

export {
  noEmptyButtonGroups,
  noInvalidReferences,
  containsOnlyValidVideoRefs,
  containsOnlyValidButtons,
  containsOnlyValidTextComponents,
  buttonsMustHaveTargetRef,
  validNextConversationTarget,
  noButtonsWithoutAGroup,
  validButtonTypes,
  hasProps,
};
