const rootSectionExists = (conversation) => {
  const { sections = [], root, alias, id } = conversation;

  // Ignore unimplemented conversation stubs
  if (!root && sections.length === 0) return;

  const rootSection = sections.find(({ id }) => root === id);

  if (!rootSection) {
    throw new Error(
      `Root section (${root}) does not exist in conversation ${id} (alias: ${alias})`,
    );
  }
};

const idIsDefined = (conversation) => {
  const { alias, id } = conversation;

  if (!id) {
    throw new Error(`ID is not defined conversation alias: ${alias}`);
  }
};

export { idIsDefined, rootSectionExists };
