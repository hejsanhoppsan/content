import { ContentValidationError } from '../errors.js';
import { noInvalidConversationRef } from './exercise.js';

describe('noInvalidConversationRef', () => {
  const data = {
    indexedContent: {
      CONVERSATIONS: {
        iDoExist: {},
      },
    },
  };

  it('should reject exercises with invalid conversation references', () => {
    const exercise = {
      id: 'blabla',
      conversations: [{ conversationId: 'iDoNotExist' }],
    };

    expect(() => noInvalidConversationRef(exercise, data)).toThrow(
      ContentValidationError,
    );
  });

  it('should accept exercises without conversation references', () => {
    expect(() => noInvalidConversationRef({}, data)).not.toThrow();
  });

  it('should accept exercises with valid conversation references', () => {
    const exercise = { conversations: [{ conversationId: 'iDoExist' }] };
    expect(() => noInvalidConversationRef(exercise, data)).not.toThrow();
  });
});
