import { prop, propEq } from 'ramda';
import { ContentValidationError } from '../errors.js';

export const noStandaloneTexts = (section) => {
  if (!section.content) return;
  const errors = [];
  const texts = section.content.filter(propEq('component', 'Text'));
  texts.forEach(() => {
    errors.push(
      new ContentValidationError(
        'Text components are not allowed outside TextGroup components',
      ),
    );
  });

  if (errors.length) {
    throw new ContentValidationError(errors.map(prop('message')).join('\n'));
  }
};

export const sectionRules = [noStandaloneTexts];
