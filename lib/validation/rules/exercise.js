import { path } from 'ramda';
import { ContentValidationError } from '../errors.js';

const getConversationById = (id, data) =>
  path(['indexedContent', 'CONVERSATIONS', id], data);

export const noInvalidConversationRef = (exercise, data) => {
  if (exercise.conversations) {
    exercise.conversations.forEach((c) => {
      if (!c.conversationId)
        throw new ContentValidationError(
          `[${exercise.id}] field 'conversationId' is undefined`,
        );

      if (!getConversationById(c.conversationId, data)) {
        throw new ContentValidationError(
          `[${exercise.id}]: Conversation ${c.conversationId} does not exist`,
        );
      }
    });
  }
};

export const exerciseRules = [noInvalidConversationRef];
