import 'colors';
import { propOr, ifElse, propEq, prop, always, find } from 'ramda';
import { ContentValidationError } from './errors.js';
import { groupContentByLang, indexContent } from './utils.js';

const createValidator =
  (select) =>
  (rules = []) =>
  (content, data) => {
    const elements = select(content, data);

    elements.forEach((e) =>
      rules.forEach((rule) => {
        // We're prepending the parent of the current node
        // because a test is more likely to be interested in
        // the immediate ancestor of a given node than
        // something near the top of the hierarchy. This makes
        // it more convenient to use:
        //
        // const [parent, grandparent, ...etc] = data.path
        const path = data.path ? [content, ...data.path] : [content];

        try {
          rule(e, { ...data, path });
        } catch (error) {
          data.reporter.error(error.message, data.lang);
        }
      }),
    );
  };

const createRootValidator =
  (reporter) =>
  (rules = []) =>
  (content) => {
    const contentByLang = groupContentByLang(content);
    contentByLang.forEach(([lang, langContent]) => {
      rules.forEach((rule) =>
        rule(langContent, {
          lang,
          indexedContent: indexContent(langContent),
          content: langContent,
          reporter,
        }),
      );
    });
    const nErrors = reporter.errors.length;
    const errorStr = `${nErrors} error(s)`;

    if (nErrors > 0)
      throw new ContentValidationError(
        `\n\n${reporter.errors.join(
          '\n\n',
        )}\n\nContent validation FAILED with ${
          reporter.errors.length > 0 ? errorStr.red : errorStr.green
        }\n`,
      );

    return content;
  };

const courseValidator = createValidator(propOr([], 'COURSES'));
const lessonValidator = createValidator(
  ifElse(propEq('wip', false), prop('lessons'), always([])), // Ignore WIP courses
);
const exerciseValidator = createValidator(propOr([], 'EXERCISES'));
const conversationValidator = createValidator(propOr([], 'CONVERSATIONS'));
const sectionValidator = createValidator(propOr([], 'sections'));
const componentValidator = createValidator(propOr([], 'content'));
const buttonGroupChildValidator = createValidator(
  ifElse(propEq('component', 'ButtonGroup'), prop('content'), always([])),
);
const conversationConnectionValidator = createValidator((element, data) => {
  if (!element.conversations) return [];

  return (
    element.conversations
      .map(({ conversationId }) =>
        find(propEq('id', conversationId), data.content.CONVERSATIONS),
      )
      // There is a rule that checks for references to
      // non-existing conversations, so filtering away
      // those references here is ok
      .filter(Boolean)
  );
});

export {
  createRootValidator,
  exerciseValidator,
  courseValidator,
  lessonValidator,
  conversationValidator,
  sectionValidator,
  componentValidator,
  buttonGroupChildValidator,
  conversationConnectionValidator,
};
