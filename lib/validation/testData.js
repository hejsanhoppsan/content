const testData = {
  CONVERSATIONS: [
    {
      en: {
        id: 'conversation-en',
        sections: [
          {
            id: 'section',
            content: [
              { component: 'SomeComponent' },
              { component: 'SomeOtherComponent' },
              { component: 'ButtonGroup', content: [{ component: 'Button' }] },
            ],
          },
        ],
      },
      sv: { id: 'conversation-sv' },
    },
  ],
  COURSES: [
    {
      en: {
        id: 'course-en',
        wip: false,
        lessons: [{ id: 'lesson-en' }],
      },
      sv: {
        id: 'course-sv',
        wip: false,
        lessons: [{ id: 'lesson-sv' }],
      },
    },
  ],
  EXERCISES: [
    {
      en: {
        id: 'exercise-en',
      },
      sv: {
        id: 'exercise-sv',
      },
    },
  ],
  nonTranslatedContent: { name: 'Robin' },
};

const indexedContent = {
  en: {
    CONVERSATIONS: {
      'conversation-en': {
        id: 'conversation-en',
        sections: {
          section: {
            id: 'section',
            content: [
              { component: 'SomeComponent' },
              { component: 'SomeOtherComponent' },

              { component: 'ButtonGroup', content: [{ component: 'Button' }] },
            ],
          },
        },
      },
    },
    COURSES: {
      'course-en': {
        id: 'course-en',
        lessons: [{ id: 'lesson-en' }],
        wip: false,
      },
    },
    EXERCISES: {
      'exercise-en': {
        id: 'exercise-en',
      },
    },
  },
  sv: {
    CONVERSATIONS: {
      'conversation-sv': { id: 'conversation-sv' },
    },
    COURSES: {
      'course-sv': {
        id: 'course-sv',
        lessons: [{ id: 'lesson-sv' }],
        wip: false,
      },
    },
    EXERCISES: {
      'exercise-sv': {
        id: 'exercise-sv',
      },
    },
  },
};

const contentByLang = {
  en: {
    CONVERSATIONS: [
      {
        id: 'conversation-en',
        sections: [
          {
            id: 'section',
            content: [
              { component: 'SomeComponent' },
              { component: 'SomeOtherComponent' },
              { component: 'ButtonGroup', content: [{ component: 'Button' }] },
            ],
          },
        ],
      },
    ],
    COURSES: [
      {
        id: 'course-en',
        lessons: [{ id: 'lesson-en' }],
        wip: false,
      },
    ],
    EXERCISES: [
      {
        id: 'exercise-en',
      },
    ],
    nonTranslatedContent: { name: 'Robin' },
  },
  sv: {
    CONVERSATIONS: [{ id: 'conversation-sv' }],
    COURSES: [
      {
        id: 'course-sv',
        lessons: [{ id: 'lesson-sv' }],
        wip: false,
      },
    ],
    EXERCISES: [
      {
        id: 'exercise-sv',
      },
    ],

    nonTranslatedContent: { name: 'Robin' },
  },
};

export { testData, indexedContent, contentByLang };
