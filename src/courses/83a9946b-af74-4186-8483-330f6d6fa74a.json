{
  "en": {
    "hideOnHome": false,
    "segments": [
      "ADULTS",
      "8be51700-fd9d-43d3-a01a-3963782535df"
    ],
    "author": {
      "nickname": ""
    },
    "name": "Dealing with stress",
    "about": {
      "body": "What would life look like if it was less stressful? In this course you'll get to explore ways to improve your ability to deal with stress and live a life that is more meaningful to you.\n\nIt will provide you with tools to change what is causing you stress. Change what you can and find new ways to relate to that which you can't affect.",
      "howThisWorks": "* This course takes eight weeks to complete\n* One lesson per week \n* One video sharing per week",
      "courseValue": "#### What will I learn?\n\n* Reduce stress \n* Increase your ability to handle challenges\n* Lead a more meaningful life",
      "longText": "What would your life look like if it was less stressful?\nThis course explores how to live a more meaningful life and become more resilient.\n\nBecoming more resilient will empower you to better deal with what’s causing you stress and relate to stress in a new way.\n\nJoin the others as they share their personal experience of going through both heavy and light-hearted exercises as an honest, willing participant.",
      "createdBy": {
        "text": "This course has been co-created by researchers and psychologists at 29k and the Karolinska Institute.",
        "profiles": [
          {
            "name": "Fredrik Livheim, PhD",
            "linkUri": "https://livskompass.se/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517825/Stress/fredrik_ijwwtj.jpg"
          },
          {
            "name": "Björn Hedensjö",
            "linkUri": "https://bjornhedensjo.se/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517870/Stress/bjo%CC%88rn_hmqjty.jpg"
          },
          {
            "name": "Daniel Ek",
            "linkUri": "https://www.hjarnkraft.com/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517924/Stress/daniel_yby8ul.jpg"
          },
          {
            "name": "Jacqueline Levi",
            "linkUri": "https://psykologipodden.libsyn.com/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628518106/Stress/jack_mkcldo.jpg"
          }
        ]
      }
    },
    "background": {
      "color": "#6C4146",
      "uri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg"
    },
    "cta": "This course is designed to help you take crucial steps towards a less stressful life by working through exercises drawn from the leading research on stress and resilience.",
    "ratingCount": 118,
    "duration": {
      "count": 8,
      "type": "week"
    },
    "coverUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg",
    "tags": [
      "Stress",
      "Anxiety"
    ],
    "id": "83a9946b-af74-4186-8483-330f6d6fa74a",
    "lessons": [
      {
        "id": "99a9ad92-e872-439c-9527-9794ea22a946",
        "name": "What is stress",
        "index": 0,
        "completionScreen": {
          "content": "Well done for completing the **First steps** lesson - you’ve just done a wonderful thing for yourself.\n\nYou are now ready for the next important step - sharing with your group."
        },
        "description": "Take your first steps towards a less stressful life.",
        "time": "20 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-first-steps",
            "title": "How stress works"
          },
          {
            "title": "Focus on the present moment",
            "conversationId": "resilience-and-stress-first-steps-2"
          },
          {
            "title": "Recovery",
            "conversationId": "resilience-and-stress-first-steps-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628079395/Stress/what-is-stress-thumbnail.jpg"
      },
      {
        "id": "7568d3bc-874c-4bf8-991e-f6065485fb9e",
        "name": "A meaningful life",
        "index": 1,
        "completionScreen": {
          "content": "Well done for completing **Meaningful Life**. Getting in touch with your values gives you a powerful compass in life.\n\nYou are now ready for the next important step - sharing with your group."
        },
        "description": "Explore what you really value in life.",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-meaningful-life",
            "title": "Balance"
          },
          {
            "title": "What gives your life meaning",
            "conversationId": "resilience-and-stress-a-meaningful-life-2"
          },
          {
            "title": "Your life values",
            "conversationId": "resilience-and-stress-a-meaningful-life-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077481/Stress/a-meaningful%20life%20thumbnail.jpg"
      },
      {
        "id": "700cc0de-24cb-4e37-a606-875d482c22d7",
        "name": "Being here and now",
        "completionScreen": {
          "content": "Well done for completing **Being here & now**.\n\nLearning to be more present is a powerful skill and the fact that your thoughts tend to wander is normal and will get less so with some practice.\n\nYou are now ready to share with your group."
        },
        "description": "Become present and practice letting go of your thoughts.",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-being-here-now",
            "title": "Being in the present moment"
          },
          {
            "title": "Common challenges",
            "conversationId": "resilience-and-stress-being-here-now-2"
          },
          {
            "title": "Me, here, now",
            "conversationId": "resilience-and-stress-being-here-now-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628243622/Stress/being%20here%20now%20thumbnail.jpg"
      },
      {
        "id": "51e0e9f2-f88a-4b5d-8bcc-00c693010475",
        "name": "Act for change",
        "time": "25 min",
        "description": "Change your life to make it more meaningful and less stressful.",
        "completionScreen": {
          "content": "Well done for completing **Act For Change**. We can’t live a life free from stress, but there is plenty we can do to reduce and eliminate unnecessary stress. \n\nYou are now ready for the next important step - sharing with your group."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-act-for-change",
            "title": "Change what isn't working"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-2",
            "title": "Stress and control"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-3",
            "title": "Problem-solving"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634569990/Stress/act-for-change-thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "6e823568-3329-457a-bdb9-7ba2b03401bd",
        "name": "Facing life",
        "time": "25 min",
        "description": "Learn to face the inevitable difficult moments in life.",
        "completionScreen": {
          "content": "\n\nYou are now ready to share. \n\nRemember the questions you are offered to share:\n\n1. Where did you feel the difficult emotions?\n2. If you stayed in the tough emotions, what happened? If opening up was too difficult, where did your thoughts go?\n3. How did it feel thinking about others going through the same tough thing?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-facing-life",
            "title": "Facing life"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-2",
            "title": "Natural stress"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-3",
            "title": "Rest in difficult emotions"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077164/Stress/facing%20life%20thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "57ea1568-b05c-43d7-b1b3-85f8f61458e5",
        "name": "Saying no — and yes",
        "time": "25 min",
        "description": "Increase resilience through healthy relationship boundaries.",
        "completionScreen": {
          "content": "\n\nYou are now ready to share with your group. Remember the things you are offered to share: \n\n1. A situation in your life where you said yes to something even though you really didn’t want to, and if it stopped you from doing something you valued.\n2. Something in your life that it would be helpful for you to say no to and your plan on how you want to respond. Also, your thoughts about what saying no will allow you to say yes to."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes",
            "title": "Saying no — and yes"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-2",
            "title": "Healthy relationships"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-3",
            "title": "The yes-trap"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-4",
            "title": "Your boundaries"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077784/Stress/saying%20yes%20and%20no%20thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "27f4f490-1fbe-4ad0-ac3b-46d0ba72df30",
        "name": "Your inner friend",
        "time": "25 min",
        "description": "Manage stress by being kinder to yourself.",
        "completionScreen": {
          "content": "You are now ready to share with your group. \n\nRemember the sharing questions: \n\nHow did you feel, writing a love letter to yourself? Did you get in touch with feelings of compassion? \n\nImagine ten years from now, could that future you express love and care for the person you are today? What would the future you say to yourself?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-the-inner-friend",
            "title": "Your inner friend"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-2",
            "title": "Find focus through breathing"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-3",
            "title": "A letter to yourself"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634570923/Stress/inner%20friend%20jpeg.jpg"
      },
      {
        "wip": false,
        "id": "b3afc34b-ced6-48fd-9785-af1ae97010e0",
        "name": "A new life",
        "time": "25 min",
        "description": "Make lasting changes in your life.",
        "completionScreen": {
          "content": "You are now ready to share with your group. Remember the questions you are offered to share: \n\n1. Why did you pick this particular value to act on? What will happen if you ignore this value.\n2. Which steps will you take now?\n3. What obstacles can you see? How can you deal with them?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-new-life",
            "title": "Reflecting on the process"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-2",
            "title": "Me a year from now"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-3",
            "title": "My plan"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634571151/Stress/a%20new%20life%20thumbnail.jpg"
      }
    ],
    "rating": 4,
    "wip": false,
    "backgroundImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg"
  },
  "sv": {
    "hideOnHome": false,
    "segments": [
      "ADULTS",
      "8be51700-fd9d-43d3-a01a-3963782535df"
    ],
    "author": {
      "nickname": ""
    },
    "name": "Hantera stress",
    "about": {
      "body": "Hur skulle livet se ut om det var mindre stressigt? I den här kursen kommer du få utforska hur du kan hantera stress bättre och leva livet mer så som du vill att det ska se ut. \n\nDu kommer även få förändra det du kan och hitta nya sätt att förhålla dig till sådant du inte kan påverka.",
      "howThisWorks": "* Den här kursen tar åtta veckor att genomföra\n* En lektion per vecka\n* En gruppdelning per vecka",
      "courseValue": "#### Vad kommer jag lära mig?\n\n* Hantera stress\n* Öka din förmåga att hantera utmaningar\n* Leva ett mer meningsfullt liv",
      "longText": "Hur skulle ditt liv se ut om det var mindre stressande?\nDenna kurs utforskar hur man kan leva ett mer meningsfullt liv och bli mer motståndskraftig.\n\nAtt bli mer motståndskraft kommer att ge dig möjlighet att bättre hantera vad som orsakar dig stress och relatera till stress på ett nytt sätt.\n\nFölj med de andra när de delar med sig av sin personliga upplevelse av att gå igenom både tunga och lättsamma övningar som en ärlig, villig deltagare.",
      "createdBy": {
        "text": "Denna kurs har skapats av forskare och psykologer vid 29k och Karolinska Institutet.",
        "profiles": [
          {
            "name": "Fredrik Livheim, PhD",
            "linkUri": "https://livskompass.se/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517825/Stress/fredrik_ijwwtj.jpg"
          },
          {
            "name": "Björn Hedensjö",
            "linkUri": "https://bjornhedensjo.se/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517870/Stress/bjo%CC%88rn_hmqjty.jpg"
          },
          {
            "name": "Daniel Ek",
            "linkUri": "https://www.hjarnkraft.com/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628517924/Stress/daniel_yby8ul.jpg"
          },
          {
            "name": "Jacqueline Levi",
            "linkUri": "https://psykologipodden.libsyn.com/",
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628518106/Stress/jack_mkcldo.jpg"
          }
        ]
      }
    },
    "featured": false,
    "background": {
      "color": "#6C4146",
      "uri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg"
    },
    "cta": "Den här kursen är utformad för att hjälpa dig ta avgörande steg mot ett mindre stressigt liv genom att arbeta med övningar från den ledande forskningen om stress.",
    "ratingCount": 118,
    "duration": {
      "count": 8,
      "type": "week"
    },
    "coverUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg",
    "tags": [
      "Stress",
      "Ångest"
    ],
    "id": "83a9946b-af74-4186-8483-330f6d6fa74a",
    "lessons": [
      {
        "id": "99a9ad92-e872-439c-9527-9794ea22a946",
        "name": "Vad är stress",
        "index": 0,
        "completionScreen": {
          "content": "Bra gjort! Du har nu gjort klart Stress – så funkar det. Du har precis gjort något fint för dig själv.\n\nDu är nu redo för nästa viktiga steg – att dela med din grupp. Kom ihåg frågorna till gruppdelningen: \n\n* Beskriv en en stressig period.\n\n1. Vilka aktiviteter eller relationer slutade du prioritera?\n2. Vilka aktiviteter eller relationer hjälpte dig? \n3. Vad gjorde du för att se till att den aktivitet du älskar blev av? Vad gjorde du för att ta hand om en relation? \n4. Lärde du dig något som kan vara till hjälp idag?"
        },
        "description": "Ta dina första steg mot ett mindre stressigt liv.",
        "time": "20 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-first-steps",
            "title": "Hur stress fungerar"
          },
          {
            "title": "Fokus på här och nu",
            "conversationId": "resilience-and-stress-first-steps-2"
          },
          {
            "title": "Återhämtning",
            "conversationId": "resilience-and-stress-first-steps-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628079395/Stress/what-is-stress-thumbnail.jpg"
      },
      {
        "id": "7568d3bc-874c-4bf8-991e-f6065485fb9e",
        "name": "Ett meningsfullt liv",
        "index": 1,
        "completionScreen": {
          "content": "Bra gjort! Du är nu klar med **Ett meningsfullt liv**. Att komma i kontakt med dina värderingar är en viktig kompass i livet.\n\nDu är nu redo för nästa steg - att dela med din grupp."
        },
        "description": "Utforska vad som verkligen är viktigt för dig i livet.",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-meaningful-life",
            "title": "Balans"
          },
          {
            "title": "Vad som är meningsfullt i ditt liv",
            "conversationId": "resilience-and-stress-a-meaningful-life-2"
          },
          {
            "title": "Dina livsvärden",
            "conversationId": "resilience-and-stress-a-meaningful-life-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077481/Stress/a-meaningful%20life%20thumbnail.jpg"
      },
      {
        "id": "700cc0de-24cb-4e37-a606-875d482c22d7",
        "name": "Att vara här och nu",
        "completionScreen": {
          "content": "Bra jobbat! Du har nu gjort klart **Att vara här och nu** .\n\nAtt lära sig att vara mer närvarande är en oerhört användbar förmåga. Det faktum att dina tankar har en tendens att vandra är normalt och med övning kommer du att bli bättre på att vara här och nu. \n\nDu är nu redo att dela med din grupp."
        },
        "description": "Bli närvarande och öva på att släppa taget om dina tankar.",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-being-here-now",
            "title": "Att vara i nuet"
          },
          {
            "title": "Vanliga utmaningar",
            "conversationId": "resilience-and-stress-being-here-now-2"
          },
          {
            "title": "Jag, här, nu",
            "conversationId": "resilience-and-stress-being-here-now-3"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628243622/Stress/being%20here%20now%20thumbnail.jpg"
      },
      {
        "id": "51e0e9f2-f88a-4b5d-8bcc-00c693010475",
        "name": "Förändra det som inte fungerar",
        "time": "25 min",
        "description": "Förändra ditt liv för att göra det mindre stressigt och mer meningsfullt.",
        "completionScreen": {
          "content": "Bra gjort! Du är nu klar med **Förändra det som inte fungerar**. Vi kan inte leva ett liv utan stress, men det finns mycket vi kan göra för att minska eller undvika onödig stress.\n\nDu är nu redo för nästa viktiga steg – att dela med din grupp."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-act-for-change",
            "title": "Förändra det som inte fungerar"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-2",
            "title": "Stress och kontroll"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-3",
            "title": "Problemlösning"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634569990/Stress/act-for-change-thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "6e823568-3329-457a-bdb9-7ba2b03401bd",
        "name": "Att möta livet",
        "time": "25 min",
        "description": "Lär dig hantera de oundvikliga svåra stunderna i livet.",
        "completionScreen": {
          "content": "Du är nu redo att dela.\n\nHär kommer frågorna vi föreslår att du delar:\n\n1. Var kände du de svåra känslorna?\n2. Om du stannade kvar i de svåra känslorna, vad hände? Om det var för svårt att öppna upp, var försvann du i dina tankar?\n3. Hur kändes det när du tänkte på att andra går igenom samma svåra saker?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-facing-life",
            "title": "Att möta livet"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-2",
            "title": "Naturlig stress"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-3",
            "title": "Vila i svåra känslor"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077164/Stress/facing%20life%20thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "57ea1568-b05c-43d7-b1b3-85f8f61458e5",
        "name": "Att säga nej — och ja",
        "time": "25 min",
        "description": "Hantera stress genom att sätta hälsosamma gränser i dina relationer. ",
        "completionScreen": {
          "content": "Du är nu redo att dela med din grupp. Vi föreslår att du delar följande:\n\n1. En situation i ditt liv där du sade ja till något fast du egentligen inte ville, och som kanske hindrade dig att göra något annat som du tycker är viktigt. \n2. En situation i ditt liv där det skulle vara bra för dig att säga nej, och hur du kan säga nej. \n3. Dina tankar kring vad detta nej skulle hjälpa dig att säga ja till för andra saker."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes",
            "title": "Att säga nej — och ja"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-2",
            "title": "Sunda relationer"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-3",
            "title": "Ja-fällan"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-4",
            "title": "Dina gränser"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1628077784/Stress/saying%20yes%20and%20no%20thumbnail.jpg"
      },
      {
        "wip": false,
        "id": "27f4f490-1fbe-4ad0-ac3b-46d0ba72df30",
        "name": "Din inre vän",
        "time": "25 min",
        "description": "Hantera stress genom att bli snällare mot dig själv.",
        "completionScreen": {
          "content": "Du är nu redo att dela med din grupp.\n\nHär är frågorna vi föreslår att du besvarar och tar med till din delning: \n\n1. Hur kändes det att skriva ett kärleksbrev till dig själv? Kom du i kontakt med din självmedkänsla?\n2. Föreställ dig att du reser tio år framåt i tiden, kan ditt framtida jag uttrycka kärlek och omtanke för den person du är idag? Vad skulle ditt framtida jag säga till dig?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-the-inner-friend",
            "title": "Din inre vän"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-2",
            "title": "Hitta fokus genom andningen"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-3",
            "title": "Skriva ett brev till dig själv"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634570923/Stress/inner%20friend%20jpeg.jpg"
      },
      {
        "wip": false,
        "id": "b3afc34b-ced6-48fd-9785-af1ae97010e0",
        "name": "Ett nytt liv",
        "time": "25 min",
        "description": "Gör långsiktiga förändringar i ditt liv.",
        "completionScreen": {
          "content": "Du är nu redo att dela med din grupp. Det här är frågorna vi föreslår att du besvarar och tar med in i delningen:\n\n1. Varför valde att agera på just det här livsvärdet? Vad skulle hända om du struntade i det här livsvärdet? \n2. Vilka konkreta steg kommer du att ta nu?\n3. Vilka möjliga hinder kan du förutse? Hur kan du hantera dem?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-new-life",
            "title": "Reflektera över din resa"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-2",
            "title": "Jag ett år från idag"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-3",
            "title": "Min plan"
          }
        ],
        "listThumbnail": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1634571151/Stress/a%20new%20life%20thumbnail.jpg"
      }
    ],
    "rating": 4,
    "backgroundImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/83a9946b-af74-4186-8483-330f6d6fa74a/new-background.jpg"
  },
  "pt": {
    "hideOnHome": false,
    "segments": [
      "ADULTS",
      "8be51700-fd9d-43d3-a01a-3963782535df"
    ],
    "author": {
      "nickname": "José Neves",
      "name": "José Neves"
    },
    "name": "Stress e Resiliência",
    "about": {
      "body": "Como seria a vida se fosse menos stressante? Este curso ajuda-te a viver uma vida com mais significado e a tornares-te mais resiliente.\n\nVai-te preparar para lidares melhor com as causas do stress e vai-te dar força para te relacionares de forma diferente com o stress.",
      "howThisWorks": "#### Como funciona este curso?\n\n* Tem a duração de 8 semanas\n* Uma lição por semana\n* Uma partilha de vídeo por semana",
      "courseValue": "#### O que vais aprender?\n\n* A reduzir o stress\n* A ser mais resiliente\n* A ter uma vida com mais significado",
      "longText": "What would your life look like if it was less stressful?\nThis course explores how to live a more meaningful life and become more resilient.\n\nBecoming more resilient will empower you to better deal with what’s causing you stress and relate to stress in a new way.\n\nJoin the others as they share their personal experience of going through both heavy and light-hearted exercises as an honest, willing participant.",
      "createdBy": {
        "profiles": [
          {
            "name": "Com José Neves"
          }
        ]
      }
    },
    "background": {
      "color": "#6C4146",
      "uri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621958192/29k%20FJN/Courses/Menu%20Thumbnails/Menu_JN_Stress_Resilience_final_woigmo.jpg"
    },
    "cta": "Dá o primeiro passo para seres mais resiliente e para uma vida menos stressante removendo as causas do stress e relacionando-te de uma forma diferente com o que não consegues mudar.",
    "ratingCount": 118,
    "duration": {
      "count": 8,
      "type": "week"
    },
    "coverUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621844608/29k%20FJN/Courses/Stress%20and%20Resilience/Photos/1coursecoverB_t3wowd.jpg",
    "tags": [
      "Stress",
      "Anxiety",
      "Ansiedade"
    ],
    "id": "83a9946b-af74-4186-8483-330f6d6fa74a",
    "lessons": [
      {
        "id": "99a9ad92-e872-439c-9527-9794ea22a946",
        "name": "Os primeiros passos",
        "index": 0,
        "completionScreen": {
          "content": "Parabéns por concluíres a lição **Os primeiros passos** - acabaste de fazer uma coisa maravilhosa para o teu bem-estar.\n\nPodes passar para a próxima etapa - partilhar com o teu grupo."
        },
        "description": "Aprende como o stress funciona para teres uma vida menos stressante.",
        "time": "20 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-first-steps",
            "title": "Os primeiros passos"
          },
          {
            "title": "Introdução ao exercício",
            "conversationId": "resilience-and-stress-first-steps-2"
          },
          {
            "title": "Exercício",
            "conversationId": "resilience-and-stress-first-steps-3"
          }
        ]
      },
      {
        "id": "7568d3bc-874c-4bf8-991e-f6065485fb9e",
        "name": "Uma vida com significado",
        "index": 1,
        "completionScreen": {
          "content": "Parabéns por completares **Uma vida com significado**. Ao entrares em contato com os teus valores, ganhas uma bússola poderosa na vida.\n\nPodes passar para a próxima fase - partilhar com o teu grupo."
        },
        "description": "Entra em contacto com o que realmente valorizas na vida",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-meaningful-life",
            "title": "Uma vida com significado"
          },
          {
            "title": "O que dá significado à tua vida?",
            "conversationId": "resilience-and-stress-a-meaningful-life-2"
          },
          {
            "title": "Exercício de modelação pessoal",
            "conversationId": "resilience-and-stress-a-meaningful-life-3"
          }
        ]
      },
      {
        "id": "700cc0de-24cb-4e37-a606-875d482c22d7",
        "name": "Estar presente, aqui e agora",
        "completionScreen": {
          "content": "Parabéns por concluíres **Estar presente, aqui e agora**.\n\nAprender a estar mais presente é uma competência poderosa e o facto de teres tendência a deixar os teus pensamentos vaguear é normal e diminuirá com alguma prática.\n\nAgora podes partilhar com o teu grupo."
        },
        "description": "Torna-te consciente do que está a acontecer agora.",
        "time": "25 min",
        "conversations": [
          {
            "conversationId": "resilience-and-stress-being-here-now",
            "title": "Estar aqui e agora"
          },
          {
            "title": "Os desafios comuns do mindfulness",
            "conversationId": "resilience-and-stress-being-here-now-2"
          },
          {
            "title": "Eu, aqui, agora",
            "conversationId": "resilience-and-stress-being-here-now-3"
          }
        ]
      },
      {
        "id": "51e0e9f2-f88a-4b5d-8bcc-00c693010475",
        "name": "Agir pela mudança",
        "time": "25 min",
        "description": "Muda a tua vida para que tenha mais significado e seja menos stressante.",
        "completionScreen": {
          "content": "Parabéns por concluíres **Agir pela mudança**. Não podemos viver uma vida livre de stress, mas há muitas coisas que podemos fazer para o reduzir e eliminar, em particular o stress desnecessário.\n\nPodes passar para a próxima fase - partilhar com o teu grupo."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-act-for-change",
            "title": "Agir pela mudança"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-2",
            "title": "Resolução de problemas perante o stress"
          },
          {
            "conversationId": "resilience-and-stress-act-for-change-3",
            "title": "Exercício 1-4"
          }
        ]
      },
      {
        "wip": false,
        "id": "6e823568-3329-457a-bdb9-7ba2b03401bd",
        "name": "Enfrentar a vida",
        "time": "25 min",
        "description": "Aprende a enfrentar os momentos inevitáveis e difíceis.",
        "completionScreen": {
          "content": "Agora estás pronta/o para partilhar.\n\nLembra-te das perguntas que podes partilhar:\n\n1. Onde sentiste as emoções difíceis?\n2. Se permaneceste nas emoções difíceis, o que aconteceu? Se não te conseguiste abrir, para onde foram os teus pensamentos?\n3. Como te sentiste ao pensar noutras pessoas que passam pelas mesmas situações difíceis?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-facing-life",
            "title": "Enfrentar a vida"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-2",
            "title": "O stress natural"
          },
          {
            "conversationId": "resilience-and-stress-facing-life-3",
            "title": "Descansa nas emoções difíceis"
          }
        ]
      },
      {
        "wip": false,
        "id": "57ea1568-b05c-43d7-b1b3-85f8f61458e5",
        "name": "Dizer não – e sim",
        "time": "25 min",
        "description": "Aumenta a tua resiliência estabelecendo limites saudáveis nas relações.",
        "completionScreen": {
          "content": "Agora já estás pronta/o para partilhar com o teu grupo. Lembra-te das coisas que podes partilhar:\n\n1. Uma situação na tua vida em que disseste sim a alguma coisa, embora na realidade não o quisesses fazer, e se isso te impediu de fazer algo que valorizavas.\n2. Alguma coisa na tua vida a que fosse útil dizeres não e qual o teu plano de ação para isso. Partilha também os teus pensamentos acerca das coisas a que vais poder dizer sim como resultado deste não."
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes",
            "title": "Dizer não – e sim"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-2",
            "title": "Relações saudáveis"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-3",
            "title": "A ratoeira do sim"
          },
          {
            "conversationId": "resilience-and-stress-saying-no-and-yes-4",
            "title": "Dizer sim ao que queres"
          }
        ]
      },
      {
        "wip": false,
        "id": "27f4f490-1fbe-4ad0-ac3b-46d0ba72df30",
        "name": "O teu amigo interior",
        "time": "25 min",
        "description": "Aprende a aumentar a tua resiliência sendo mais amigo de ti própria/o",
        "completionScreen": {
          "content": "Podes passar para a partilha com o teu grupo. \n\nLembra-te destas perguntas para a partilha: \n\nComo te sentiste ao escrever uma carta de amor para ti própria/o? Entraste em contacto com sentimentos de compaixão? \n\nImagina-te daqui a dez anos. Esse futuro tu poderia expressar amor e carinho por quem és hoje? O que diria o teu futuro eu ao teu eu presente?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-the-inner-friend",
            "title": "O teu amigo interior"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-2",
            "title": "A tua voz interior"
          },
          {
            "conversationId": "resilience-and-stress-the-inner-friend-3",
            "title": "A tua carta de amor"
          }
        ]
      },
      {
        "wip": false,
        "id": "b3afc34b-ced6-48fd-9785-af1ae97010e0",
        "name": "Uma vida nova",
        "time": "25 min",
        "description": "Fazer mudanças duradouras na tua vida",
        "completionScreen": {
          "content": "Agora já podes partilhar com o teu grupo. Lembra-te das perguntas que podes partilhar:\n\n1. Porque escolheste este valor em particular para agir? O que acontecerá se ignorares esse valor?\n2. Que passos darás agora?\n3. Que obstáculos antecipas? Como podes lidar com eles?"
        },
        "conversations": [
          {
            "conversationId": "resilience-and-stress-a-new-life",
            "title": "Uma vida nova"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-2",
            "title": "A vida a partir de agora"
          },
          {
            "conversationId": "resilience-and-stress-a-new-life-3",
            "title": "Eu daqui a um ano"
          }
        ]
      }
    ],
    "rating": 4,
    "wip": false,
    "backgroundImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621844608/29k%20FJN/Courses/Stress%20and%20Resilience/Photos/1coursecoverB_t3wowd.jpg"
  }
}