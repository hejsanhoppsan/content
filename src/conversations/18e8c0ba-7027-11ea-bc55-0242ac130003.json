{
  "en": {
    "id": "18e8c0ba-7027-11ea-bc55-0242ac130003",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Acceptance",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Acceptance_ydm3dg.png",
        "title": "Acceptance",
        "content": "*15 minutes*\n\nMetaphors can help us get a new perspective on how different aspects of our lives relate to each other.\n\nIn this exercise you’ll learn more about acceptance.\n",
        "buttons": [
          {
            "component": "Button",
            "content": "Let’s do this",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "The Acceptance Slider",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/exercises/18e8c0ba-7027-11ea-bc55-0242ac130003/acceptance.png",
        "title": "The Acceptance Slider",
        "content": "The slider is a metaphor about acceptance.\n\nImagine that we have two invisible controls inside us, like the volume and tone controls from an old ’70s stereo.\n\nThe left-hand “acceptance slider” represents your willingness to live with our emotions. You have complete control of this slider. For example, every time you accept a difficult emotion, it’s pushed all the way up to the top. And every time you get angry of frustrated with yourself for feeling a difficult emotion, the slider is set low. This is because you’re trying to fight, control or flee that emotion.\n\nThe slider to the right is the “difficult-emotions slider.” Unfortunately, we have no control over this one; it has a mind of its own.",
        "buttons": [
          {
            "component": "Button",
            "content": "Let’s do this",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "The Acceptance Slider",
        "topImageUri": "",
        "title": "The Acceptance Slider",
        "content": "The acceptance slider has a secret in-built function.\n\nWhen the acceptance slider is all the way down, (e.g. when we judge or resist our emotions), the difficult-emotions slider goes all the way up. It can almost feel as if it gets stuck: no matter how hard we try to move the slider, it refuses to budge, only intensifying our emotions.\n\nOn the other hand, when we open to our emotions, no matter how difficult, we push the acceptance slider up again and experience the opposite effect - the difficult emotions slider gets unstuck and can move up freely again according to our actual feelings instead of our interpretations of those feelings.",
        "buttons": [
          {
            "component": "Button",
            "content": "Let’s do this",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "editorialWithImage",
        "alias": "section4",
        "topImageUri": "",
        "title": "",
        "content": "Our thoughts and emotions work depending on whether or not we accept them. Maybe you’ve heard the saying “What you resist, persists.”\n\nAlthough we have no control of the difficult emotions, as soon as we stop resisting them and instead create space, they are free to move again.",
        "buttons": [
          {
            "component": "Button",
            "content": "Let’s do this",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "writing",
        "alias": "Exercise",
        "title": "Exercise",
        "time": "",
        "question": "",
        "longDescription": "Think about your day so far. How often have you done anything to reduce, change, control or otherwise affect the experience of discomfort?",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "writing",
        "alias": "Turn up your acceptance slider",
        "title": "Turn up your acceptance slider",
        "time": "",
        "question": "",
        "longDescription": "Now it’s over to you: Take a look at your own acceptance slider - where is it at right now?\n\nDecide how much you wish to turn it up. It doesn’t matter where you stand right now, the point is to increase your level of acceptance of difficult emotions.\n\nIn doing so you are making an active choice to open up to what’s out of your control. For example you choose to accept that you’re angry or sad about not being able to leave your home.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "74b0b780-8e25-4e42-bee1-58cee4805c9a"
          }
        ]
      },
      {
        "type": "sharingExercise",
        "previewSharings": true,
        "enableEarlySkip": true,
        "id": "74b0b780-8e25-4e42-bee1-58cee4805c9a",
        "question": "How do you think choosing to accept your emotion, instead of resisting it, can affect you?",
        "exampleText": "I think it can open other possibilities for me.",
        "alias": "sharing wall q",
        "nextSection": "section7"
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Well done!",
        "topImageUri": "",
        "title": "Well done!",
        "content": "Well done for completing this exercise.\n\nNow that you’ve set a new, higher level of acceptance and let go of the temptation to resist, flee or control your emotions, you will start to experience your emotions for what they are and invest your energy for what you can control.",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Finish"
          }
        ]
      }
    ],
    "alias": "Exercise Acceptance"
  },
  "sv": {
    "id": "18e8c0ba-7027-11ea-bc55-0242ac130003",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Acceptans",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Acceptance_ydm3dg.png",
        "title": "Acceptans",
        "content": "*15 minuter*\n\nMetaforer kan ibland hjälpa oss att komma fram till nya insikter om hur livets olika delar hänger ihop.\n\nI den här övningen kommer du att lära dig om acceptans.",
        "buttons": [
          {
            "component": "Button",
            "content": "Nu kör vi",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Acceptans-kontrollen",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/exercises/18e8c0ba-7027-11ea-bc55-0242ac130003/acceptance.png",
        "title": "Acceptans-kontrollen",
        "content": "Kontrollen är en metafor för acceptans\n\nTänk dig att du inombords har två olika kontroller, ungefär som de ton- och volymknappar man kan hitta på en gammal stereo från 70-talet.\n\nKontrollen på vänster sida, **acceptans-kontrollen**, representerar din villighet att uppleva alla dina känslor. Du har full kontroll över den här knappen. Väljer du att acceptera en svår känsla, till exempel, då trycks kontrollen hela vägen upp. Blir du istället arg och frustrerad på dig själv för att du känner på ett visst sätt går den ner. Alla försök att kontrollera, kämpa emot, eller fly undan känslan betyder att kontrollen är nere på noll .\n\nTill höger är kontrollen för **känslor**. Den har vi tyvärr ingen kontroll över - den gör som den vill.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "Acceptans-slidern",
        "topImageUri": "",
        "title": "Acceptans-kontrollen",
        "content": "Men acceptans-kontrollen har en **dold** funktion.\n\nNär acceptans-kontrollen är i **botten** (till exempel när vi stretar emot våra känslor), då åker kontrollen för svåra känslor hela vägen **upp**. Det kan till och med verka som att den **fastnat**: hur vi än försöker går det inte att flytta på kontrollen. Den vill inte. Och det **förstärker** bara våra känslor.\n\nMen öppnar vi upp för våra känslor, även de svåra, och därmed trycker upp acceptans-kontrollen då upplever vi **motsatt** effekt - att kontrollen för svåra känslor lossnar och istället kan röra sig i enlighet med hur vi faktiskt känner snarare än våra **tolkningar** av känslorna.",
        "buttons": [
          {
            "component": "Button",
            "content": "Nästa",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "editorialWithImage",
        "alias": "section4",
        "topImageUri": "",
        "title": "",
        "content": "Våra **känslor och tankegångar** påverkas till står del av **hur villiga** vi är att acceptera dom.\n\nVi kan inte kontrollera svåra känslor, men om vi kan **ge dem tid och plats**, istället för att brottas med dem, kan de röra sig fritt igen.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "writing",
        "alias": "Övning",
        "title": "Övning",
        "time": "",
        "question": "",
        "longDescription": "Tänk på din dag så här långt. Hur ofta har du gjort något som har minskat, ändrat, kontrollerat eller på annat vis påverkat känslan av obehag?",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "writing",
        "alias": "Vrid upp din acceptans-kontroll",
        "title": "Dra upp din acceptans-kontroll",
        "time": "",
        "question": "",
        "longDescription": "Nu är det din tur: Titta på din egen acceptans-kontroll – var är den just nu?\n\nBestäm dig för hur mycket du skulle vilja dra upp kontrollen. Hur du känner just nu är mindre viktigt, syftet är att **höja** din acceptans för svåra känslor.\n\nI och med detta gör du ett **aktivt** val att öppna för det som ligger **bortom din kontroll.** Du kan till exempel **acceptera** **att du är arg eller ledsen** för att du inte kan göra något som du hade längtat efter.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "section7"
          }
        ]
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Bra gjort!",
        "topImageUri": "",
        "title": "Bra gjort!",
        "content": "Nu när du har satt en ny, högre nivå av accepants och släppt impulsen att kämpa emot, fly eller kontrollera dina känslor kommer du att börja uppleva dina känslor som de är och lägga din energi på det du kan påverka.",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Avsluta"
          }
        ]
      }
    ],
    "alias": "Exercise Acceptance"
  },
  "pt": {
    "id": "18e8c0ba-7027-11ea-bc55-0242ac130003",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Aceitação",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Acceptance_ydm3dg.png",
        "title": "Aceitação",
        "content": "*15 minutos*\n\nAs metáforas podem ajudar-nos a obter uma nova perspetiva de como diferentes aspetos das nossas vidas estão relacionados entre si.\n\nNeste exercício, vais aprender mais sobre a aceitação.",
        "buttons": [
          {
            "component": "Button",
            "content": "Vamos a isto",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Regulador de Aceitação",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621437591/29k%20FJN/Images/Slicers_awfezv.png",
        "title": "Regulador de Aceitação",
        "content": "O regulador é uma metáfora sobre aceitação.\n\nImagina que temos dois reguladores invisíveis dentro de nós, como os de volume e tom de uma velha aparelhagem dos anos 70. \n\nO da esquerda, \"o regulador da aceitação\", representa a tua disponibilidade para viver com as tuas emoções. Tu tens o controlo total deste regulador. Por exemplo, sempre que aceitas uma emoção difícil, ele aumenta até ao limite. E, de cada vez que te zangas ou sentes frustração por sentires uma emoção difícil, o regulador desce até ao mínimo. Isto acontece porque estás a tentar resistir, controlar ou fugir dessa emoção.\n\nO regulador da direita é o \" regulador das emoções difíceis\". Infelizmente, não o controlamos, ele tem vontade própria.",
        "buttons": [
          {
            "component": "Button",
            "content": "Vamos a isto",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "O Regulador de Aceitação",
        "topImageUri": "",
        "title": "O Regulador de Aceitação",
        "content": "O regulador da aceitação tem uma função secreta incorporada.\n\nQuando o regulador da aceitação está no mínimo (por exemplo, quando avaliamos ou resistimos às nossas emoções), o regulador das emoções difíceis vai até ao máximo. É quase como sentíssemos que ele fica preso: por muito que tentemos mover o regulador, ele recusa-se a ceder, intensificando as nossas emoções.\n\nPor outro lado, quando nos abrimos às nossas emoções, por mais difícil que seja, empurramos o regulador da aceitação para cima e experienciamos o efeito oposto - o regulador das emoções difíceis solta-se e pode mover-se livremente de acordo com os nossos sentimentos reais em vez das nossas interpretações desses sentimentos.",
        "buttons": [
          {
            "component": "Button",
            "content": "Vamos a isto",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "editorialWithImage",
        "alias": "Aquilo a que resistes persiste",
        "topImageUri": "",
        "title": "Aquilo a que resistes persiste",
        "content": "Os nossos pensamentos e emoções funcionam de acordo com a nossa aceitação ou não aceitação. Talvez já tenhas ouvido o ditado: \n\n\"Aquilo a que resistes, persiste\"\n\nEmbora não tenhamos controlo sobre as emoções difíceis, assim que deixamos de lhes resistir e, em vez disso, lhes damos espaço, elas ficam livres para se moverem outra vez.",
        "buttons": [
          {
            "component": "Button",
            "content": "Vamos a isto",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "writing",
        "alias": "Exercício",
        "title": "Exercício",
        "time": "",
        "question": "",
        "longDescription": "Pensa sobre o teu dia até agora. Quantas vezes fizeste alguma coisa para reduzir, mudar, controlar ou de alguma forma influenciar a experiência de desconforto?",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "writing",
        "alias": "Aumenta o teu regulador de aceitação ",
        "title": "Aumenta o teu regulador de aceitação ",
        "time": "",
        "question": "",
        "longDescription": "Agora é contigo!\n\nOlha para o teu próprio regulador da aceitação. Onde é que ele está neste momento? \n\nDecide o quanto desejas aumentá-lo. Não importa onde estás neste momento, o objetivo é aumentar o teu nível de aceitação de emoções difíceis. \n\nAo fazê-lo, estás a fazer uma escolha ativa para te abrires ao que está fora do teu controlo. Por exemplo, escolhe aceitar que estás zangada/o ou triste por não poderes sair de casa.\n\nComo é que a escolha de aceitar a tua emoção, em vez de lhe resistires, te afeta?",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "2bc7d7e7-44fb-4acb-a258-48fc16cc4788"
          }
        ]
      },
      {
        "type": "sharingExercise",
        "previewSharings": true,
        "enableEarlySkip": true,
        "id": "2bc7d7e7-44fb-4acb-a258-48fc16cc4788",
        "alias": "sharing wall q",
        "question": "O que achas que pode mudar na tua vida se aceitares as tuas emoções em vez de lhes resistires?",
        "exampleText": "Penso que vai abrir um conjunto de oportunidades para ter uma vida mais positiva e saudável.",
        "nextSection": "section7"
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Parabéns!",
        "topImageUri": "",
        "title": "Parabéns!",
        "content": "Parabéns por completares este exercício. \n\nAgora que estabeleceste um novo e maior nível de aceitação e largaste a tentação de resistir, fugir ou controlar as tuas emoções, vais começar a experienciar as tuas emoções tal como elas são e a investir a tua energia naquilo que podes controlar.",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Terminar"
          }
        ]
      }
    ],
    "alias": "Exercise Acceptance"
  }
}