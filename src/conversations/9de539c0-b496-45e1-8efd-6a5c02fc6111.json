{
  "en": {
    "id": "9de539c0-b496-45e1-8efd-6a5c02fc6111",
    "alias": "Stress YA: Change what isn't working 2",
    "root": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
        "alias": "Problem-solving",
        "title": "Problem-solving",
        "content": "When we’re feeling stressed our problems can seem vague. Confusing. It can be difficult to come up with possible solutions. \\\n\\\nAt the same time, our brains are affected — our **ability to solve problems** actually gets *worse* when we're very stressed.",
        "buttons": [
          {
            "component": "Button",
            "content": "More",
            "sectionId": "77aae570-b274-41b1-a67c-30b4fce4bdbb"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "77aae570-b274-41b1-a67c-30b4fce4bdbb",
        "alias": "Change step by step",
        "title": "Change step by step",
        "content": "Now you’ll get to explore a situation that is currently stressing you out. \n\nYou'll map out what you can do differently — from coming up with **solutions** to planning **concrete** **steps**.\n\nBefore we begin, estimate how stressed you are feeling **right now** on a scale between 0 (not at all) to 10 (super stressed). Write down your answer.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "f0bd51ab-91c7-4c31-b813-0aa381f3b4a8"
          }
        ]
      },
      {
        "type": "defaultSection",
        "id": "f0bd51ab-91c7-4c31-b813-0aa381f3b4a8",
        "alias": "Videos",
        "content": [
          {
            "component": "VideoStories",
            "content": [
              {
                "title": "Emilia",
                "duration": 51,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/emilia.mp4",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758104/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-emilia-en_cc36g1.vtt",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/emilia.png"
              },
              {
                "title": "Julie",
                "duration": 46,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/julie.mp4",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/julie.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758160/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-julie-en_ac0qug.vtt"
              },
              {
                "title": "Amir",
                "duration": 53,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/amir.mp4",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/amir.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758228/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-amir-en_tnncvm.vtt"
              },
              {
                "title": "Hillevi",
                "duration": 46,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/hillevi.mp4",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758301/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-hillevi-en_npuyp2.vtt",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/hillevi.png"
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "b2d03b84-201f-4220-ac11-c90a23a1da22"
              }
            ]
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "b2d03b84-201f-4220-ac11-c90a23a1da22",
        "alias": "Exercise - step 1",
        "content": "1. Define a problem that is stressing you out right now. Try to be as concrete and honest as possible.\n\n   Examples like \"*I feel inadequate*\" can easily become too **vague**, while \"*I can’t focus on my homework because I often check my mobile instead*\" is more **concrete**.\n\n   Write down your answer and save it, you’ll need it later.",
        "title": "Exercise",
        "buttons": [
          {
            "component": "Button",
            "content": "Keep going",
            "sectionId": "d38f4877-3e44-4f29-82a2-a5a2c891d858"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "d38f4877-3e44-4f29-82a2-a5a2c891d858",
        "content": "2. Now, let’s come up with possible **solutions** to your problem. This is the time for all wild, crazy, boring, crazy and fun ideas, so let’s go. \n\n   Write down all your ideas.",
        "alias": "Step two",
        "title": "Solutions",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "c515e14f-9a95-4306-80bc-f9e927b3badc"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "c515e14f-9a95-4306-80bc-f9e927b3badc",
        "alias": "Step three",
        "title": "Score",
        "content": "3. Give each idea a score from 0 to 10, depending on how well you think each idea could solve the problem (10 = likely to work, 0 = unlikely to work).\n\n   Write down your answers again.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "67a781dc-c63d-441a-94f1-a81f6431eeb8"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "67a781dc-c63d-441a-94f1-a81f6431eeb8",
        "alias": "Step four",
        "title": "Try the solution",
        "content": "4. Choose an idea to **test**.\n5. Decide **when** you're going to test it (within the next week).\n6. Set a **goal**, or decide how you're going to evaluate your idea. The important thing is that it’s possible to **measure** whether you've reached your goal or not (*with a simple yes, no, or partly*).\n\n*Examples of goals can be: submitted the homework, applied for two jobs.*",
        "buttons": [
          {
            "component": "Button",
            "content": "More",
            "sectionId": "2a7e0450-66a4-42c5-8185-73c676ef5a2d"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "2a7e0450-66a4-42c5-8185-73c676ef5a2d",
        "alias": "Step five",
        "title": "Remember and evalute",
        "content": "6. Put a reminder on your phone in exactly one week from now. **Evaluate** your proposed solution:\n\n   * Did you reach your goal? Are you happy with your solution? If so, what has become better in your life and how?\n   * If not, try another idea from your list or adjust the idea you tested and try again. Sometimes we need to test several solutions before we find one that works.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "23e18229-b608-4bdf-8350-0816d94592ef"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "23e18229-b608-4bdf-8350-0816d94592ef",
        "content": "Think about the problem and the solution you chose. You've now taken control over the situation, and are ready to change the things you can control.\n\nEstimate how stressed you're feeling right now, on a scale between 0 (not at all) and 10 (super stressed). Write down the answer, and compare it to the number you had before the exercise.\n\nMaybe you feel a little calmer. Perhaps you will feel a little calmer after testing your solution. \n\n*Regardless, you're on your way to solve a problem!*",
        "alias": "x",
        "buttons": [
          {
            "component": "Button",
            "content": "Done",
            "sectionId": "8875b873-2dcd-4daa-92e8-4619f7f3c70b"
          }
        ],
        "title": "Score again"
      },
      {
        "type": "editorialWithImage",
        "id": "8875b873-2dcd-4daa-92e8-4619f7f3c70b",
        "alias": "Good job!",
        "title": "Good job!",
        "content": "You are now done with **Change what isn't working.**\n\nThe problem-solving steps you've learned are:\n\n* Sort out a problem that you have some control over\n* Define the problem clearly\n* Come up with solutions\n* Score ideas 0-10\n* Choose an idea and plan when you will test it out\n* Evaluate",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Finished"
          }
        ]
      }
    ]
  },
  "sv": {
    "id": "9de539c0-b496-45e1-8efd-6a5c02fc6111",
    "alias": "Stress YA: Change what isn't working 2",
    "root": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
        "alias": "Problemlösning",
        "title": "Problemlösning",
        "content": "När vi är stressade kan våra problem kännas otydliga. Förvirrande. Det kan vara svårt att komma på möjliga lösningar. \n\nSamtidigt blir vår hjärna påverkad —  vår **förmåga att lösa problem** blir faktiskt *sämre* när vi är väldigt stressade.",
        "buttons": [
          {
            "component": "Button",
            "content": "Mer",
            "sectionId": "77aae570-b274-41b1-a67c-30b4fce4bdbb"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "77aae570-b274-41b1-a67c-30b4fce4bdbb",
        "alias": "Förändra steg för steg",
        "title": "Förändra steg för steg",
        "content": "Nu kommer du få utforska en situation som stressar dig just nu. \n\nDu kommer få kartlägga vad du kan göra annorlunda — från att komma på **lösningar**, till att planera **konkreta** **steg**.\n\nInnan vi börjar: skatta hur stressad du känner dig just nu, på en skala mellan noll (inte alls) och tio (superstressad). Skriv ner ditt svar.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "2662405b-9868-4e16-9694-8613b3f86a13"
          }
        ]
      },
      {
        "type": "defaultSection",
        "id": "2662405b-9868-4e16-9694-8613b3f86a13",
        "alias": "Videos",
        "content": [
          {
            "component": "VideoStories",
            "content": [
              {
                "title": "Emilia",
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/emilia.mp4",
                "duration": 52,
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/emilia.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1621607233/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-emilia_doce4u.vtt"
              },
              {
                "title": "Julie",
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/julie.mp4",
                "duration": 46,
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/julie.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1621607233/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-julie_e7q32z.vtt"
              },
              {
                "title": "Amir",
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/amir.mp4",
                "duration": 53,
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/amir.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1621607233/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-amir_e4omrt.vtt"
              },
              {
                "title": "Hillevi",
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/hillevi.mp4",
                "duration": 46,
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/hillevi.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1621607233/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-hillevi_fuiccf.vtt"
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Fortsätt",
                "sectionId": "b2d03b84-201f-4220-ac11-c90a23a1da22"
              }
            ]
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "b2d03b84-201f-4220-ac11-c90a23a1da22",
        "alias": "Övning - steg ett",
        "content": "1. Definiera ett problem som stressar dig just nu. Försök vara så konkret och ärlig som möjligt. \n\nExempel som  *”Jag känner mig otillräcklig”* blir lätt för otydliga, medan *\"Jag kan inte fokusera på mina läxor eftersom jag ofta kollar på mobilen istället\"* är mer konkret.\n\nSkriv ner ditt svar och spara det, du kommer att behöva det igen.",
        "title": "Övning",
        "buttons": [
          {
            "component": "Button",
            "content": "Gå vidare",
            "sectionId": "d38f4877-3e44-4f29-82a2-a5a2c891d858"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "d38f4877-3e44-4f29-82a2-a5a2c891d858",
        "content": "2. Kom på en massa tänkbara lösningar på ditt problem. Här finns det plats för vilda, galna, tråkiga, tokiga och roliga idéer, så kör på. \n\n   Skriv ner *alla* dina idéer.",
        "alias": "Steg två",
        "title": "Lösningar",
        "buttons": [
          {
            "component": "Button",
            "content": "Nästa",
            "sectionId": "c515e14f-9a95-4306-80bc-f9e927b3badc"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "c515e14f-9a95-4306-80bc-f9e927b3badc",
        "alias": "Steg tre",
        "title": "Poängsätt",
        "content": "3. Ge varje idé en poäng mellan noll och tio, beroende på hur bra du tror att respektive idé skulle kunna lösa problemet *(10 = kommer sannolikt funka, 0 = osannolikt att det kommer funka).* \n\n   Skriv återigen ner dina svar.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätt",
            "sectionId": "67a781dc-c63d-441a-94f1-a81f6431eeb8"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "67a781dc-c63d-441a-94f1-a81f6431eeb8",
        "alias": "Steg fyra och fem",
        "title": "Testa lösningen",
        "content": "4. Välj en idé att **testa**, och en tidpunkt när du ska testa den (inom närmaste veckan). \n5. Bestäm ett **mål**, eller hur du ska utvärdera din idé. Det viktiga här är att det är möjligt att mäta om du har nått ditt mål (*med ett enkelt ja, nej, eller delvis*). \n\n*Exempel på mål kan vara: lämnat in läxan, sökt två jobb.*",
        "buttons": [
          {
            "component": "Button",
            "content": "Jag är klar",
            "sectionId": "2a7e0450-66a4-42c5-8185-73c676ef5a2d"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "2a7e0450-66a4-42c5-8185-73c676ef5a2d",
        "alias": "Steg sex",
        "title": "Påminn och utvärdera",
        "content": "6. Sätt en påminnelse i mobilen om exakt en vecka. Utvärdera ditt lösningsförslag:\n\n   * Nådde du ditt mål? Är du nöjd med din lösning? Om ja, vad har blivit bättre i ditt liv och hur?\n   * Om nej, testa en annan idé från din lista eller ändra lite på den idé du testade och prova igen. Ibland behöver vi testa flera lösningar innan vi hittar en som fungerar.",
        "buttons": [
          {
            "component": "Button",
            "content": "Färdig",
            "sectionId": "23e18229-b608-4bdf-8350-0816d94592ef"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "23e18229-b608-4bdf-8350-0816d94592ef",
        "content": "Tänk på problemet och lösningen du valde. Du har nu tagit mer kontroll över situationen, och är redo att förändra någonting du kan påverka. \n\nSkatta återigen hur stressad du känner dig just nu, på en skala mellan noll (inte alls) och tio (superstressad). Skriv ned svaret, och jämför det med siffran du satte innan övningen. \n\nKanske känner du dig lite lugnare. Kanske kommer du känna dig lite lugnare efter du testat din lösning. \n\n*Oavsett är du på väg mot att lösa ett problem!*",
        "alias": "x",
        "buttons": [
          {
            "component": "Button",
            "content": "Färdig",
            "sectionId": "8875b873-2dcd-4daa-92e8-4619f7f3c70b"
          }
        ],
        "title": "Skatta igen"
      },
      {
        "type": "editorialWithImage",
        "id": "8875b873-2dcd-4daa-92e8-4619f7f3c70b",
        "alias": "Bra gjort!",
        "title": "",
        "content": "Bra gjort! Du är nu klar med **Förändra det som inte fungerar.**\n\nStegen för problemlösning som du har lärt dig är:\n\n* Sortera ut ett problem som du har viss kontroll över\n* Definiera problemet tydligt\n* Hitta på lösningar\n* Poängsätt idéerna 0-10\n* Välj en idé och planera in när du ska testa den\n* Utvärdera",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Slut"
          }
        ]
      }
    ]
  },
  "pt": {
    "id": "9de539c0-b496-45e1-8efd-6a5c02fc6111",
    "alias": "Stress YA: Change what isn't working 2",
    "root": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
    "sections": [
      {
        "type": "editorialWithImage",
        "id": "eaca13f4-ec84-42c8-a2d9-3964d581224c",
        "alias": "Problem-solving",
        "title": "Problem-solving",
        "content": "When we’re feeling stressed our problems can seem vague. Confusing. It can be difficult to come up with possible solutions. \\\n\\\nAt the same time, our brains are affected — our **ability to solve problems** actually gets *worse* when we're very stressed.",
        "buttons": [
          {
            "component": "Button",
            "content": "More",
            "sectionId": "77aae570-b274-41b1-a67c-30b4fce4bdbb"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "77aae570-b274-41b1-a67c-30b4fce4bdbb",
        "alias": "Change step by step",
        "title": "Change step by step",
        "content": "Now you’ll get to explore a situation that is currently stressing you out. \n\nYou'll map out what you can do differently — from coming up with **solutions** to planning **concrete** **steps**.\n\nBefore we begin, estimate how stressed you are feeling **right now** on a scale between 0 (not at all) to 10 (super stressed). Write down your answer.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "f0bd51ab-91c7-4c31-b813-0aa381f3b4a8"
          }
        ]
      },
      {
        "type": "defaultSection",
        "id": "f0bd51ab-91c7-4c31-b813-0aa381f3b4a8",
        "alias": "Videos",
        "content": [
          {
            "component": "VideoStories",
            "content": [
              {
                "title": "Emilia",
                "duration": 51,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/emilia.mp4",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758104/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-emilia-en_cc36g1.vtt",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/emilia.png"
              },
              {
                "title": "Julie",
                "duration": 46,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/julie.mp4",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/julie.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758160/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-julie-en_ac0qug.vtt"
              },
              {
                "title": "Amir",
                "duration": 53,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/amir.mp4",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/amir.png",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758228/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-amir-en_tnncvm.vtt"
              },
              {
                "title": "Hillevi",
                "duration": 46,
                "videoUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/videos/ya-stress-lesson-3/hillevi.mp4",
                "subtitleUri": "https://res.cloudinary.com/twentyninek/raw/upload/q_auto,t_global/v1623758301/Hantera%20Stress%20YA/subtitles/Fra%CC%8Aga_3-hillevi-en_npuyp2.vtt",
                "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/ya-stress-lesson-3/hillevi.png"
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "b2d03b84-201f-4220-ac11-c90a23a1da22"
              }
            ]
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "b2d03b84-201f-4220-ac11-c90a23a1da22",
        "alias": "Exercise - step 1",
        "content": "1. Define a problem that is stressing you out right now. Try to be as concrete and honest as possible.\n\n   Examples like \"*I feel inadequate*\" can easily become too **vague**, while \"*I can’t focus on my homework because I often check my mobile instead*\" is more **concrete**.\n\n   Write down your answer and save it, you’ll need it later.",
        "title": "Exercise",
        "buttons": [
          {
            "component": "Button",
            "content": "Keep going",
            "sectionId": "d38f4877-3e44-4f29-82a2-a5a2c891d858"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "d38f4877-3e44-4f29-82a2-a5a2c891d858",
        "content": "2. Now, let’s come up with possible **solutions** to your problem. This is the time for all wild, crazy, boring, crazy and fun ideas, so let’s go. \n\n   Write down all your ideas.",
        "alias": "Step two",
        "title": "Solutions",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "c515e14f-9a95-4306-80bc-f9e927b3badc"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "c515e14f-9a95-4306-80bc-f9e927b3badc",
        "alias": "Step three",
        "title": "Score",
        "content": "3. Give each idea a score from 0 to 10, depending on how well you think each idea could solve the problem (10 = likely to work, 0 = unlikely to work).\n\n   Write down your answers again.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "67a781dc-c63d-441a-94f1-a81f6431eeb8"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "67a781dc-c63d-441a-94f1-a81f6431eeb8",
        "alias": "Step four",
        "title": "Try the solution",
        "content": "4. Choose an idea to **test**.\n5. Decide **when** you're going to test it (within the next week).\n6. Set a **goal**, or decide how you're going to evaluate your idea. The important thing is that it’s possible to **measure** whether you've reached your goal or not (*with a simple yes, no, or partly*).\n\n*Examples of goals can be: submitted the homework, applied for two jobs.*",
        "buttons": [
          {
            "component": "Button",
            "content": "More",
            "sectionId": "2a7e0450-66a4-42c5-8185-73c676ef5a2d"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "2a7e0450-66a4-42c5-8185-73c676ef5a2d",
        "alias": "Step five",
        "title": "Remember and evalute",
        "content": "6. Put a reminder on your phone in exactly one week from now. **Evaluate** your proposed solution:\n\n   * Did you reach your goal? Are you happy with your solution? If so, what has become better in your life and how?\n   * If not, try another idea from your list or adjust the idea you tested and try again. Sometimes we need to test several solutions before we find one that works.",
        "buttons": [
          {
            "component": "Button",
            "content": "Next",
            "sectionId": "23e18229-b608-4bdf-8350-0816d94592ef"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "23e18229-b608-4bdf-8350-0816d94592ef",
        "content": "Think about the problem and the solution you chose. You've now taken control over the situation, and are ready to change the things you can control.\n\nEstimate how stressed you're feeling right now, on a scale between 0 (not at all) and 10 (super stressed). Write down the answer, and compare it to the number you had before the exercise.\n\nMaybe you feel a little calmer. Perhaps you will feel a little calmer after testing your solution. \n\n*Regardless, you're on your way to solve a problem!*",
        "alias": "x",
        "buttons": [
          {
            "component": "Button",
            "content": "Done",
            "sectionId": "8875b873-2dcd-4daa-92e8-4619f7f3c70b"
          }
        ],
        "title": "Score again"
      },
      {
        "type": "editorialWithImage",
        "id": "8875b873-2dcd-4daa-92e8-4619f7f3c70b",
        "alias": "Good job!",
        "title": "Good job!",
        "content": "You are now done with **Change what isn't working.**\n\nThe problem-solving steps you've learned are:\n\n* Sort out a problem that you have some control over\n* Define the problem clearly\n* Come up with solutions\n* Score ideas 0-10\n* Choose an idea and plan when you will test it out\n* Evaluate",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Finished"
          }
        ]
      }
    ]
  }
}
