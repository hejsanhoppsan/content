{
  "en": {
    "id": "EiRy9KYDgNt9MCxjoFsFfF-1",
    "alias": "Self compassion - Inner critic 1",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "The inner critic",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "The inner critic",
        "content": "> \"I can’t believe I go through life with this voice and still get good things done…\"\n\nRachel Brathen",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "be576a58-f29f-4b56-94d3-8e38c0a112e8"
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "be576a58-f29f-4b56-94d3-8e38c0a112e8",
        "content": "Today is the first step on your journey towards a more constructive and loving relationship with yourself. In this first lesson you will get a feeling of what self-compassion is and how it can help you in your life.",
        "infoItems": [
          {
            "title": "The Research",
            "description": "This course is inspired by the evidence-based curriculum Mindful Self-Compassion developed by Kristen Neff, PhD and Cris Germer, PhD. The psychologists at 29k in collaboration with teachers and researchers from the Center for Mindful Self-Compassion have crafted this course."
          }
        ],
        "alias": "Introduction",
        "title": "Introduction",
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Sounds great",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "What is the inner critic?",
        "title": "What is the inner critic?",
        "content": "In our everyday lives we often expect ourselves to be perfect at several roles at the same time. The amazing friend. The loving partner. The great co-worker.\n\nEventually, we just don’t add up and then **we judge ourselves** for not being better or doing more.",
        "buttons": [
          {
            "component": "Button",
            "content": "Tell me more",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "editorialWithImage",
        "alias": "Inner Critic 2",
        "topImageUri": "",
        "title": "",
        "content": "The language we use towards ourselves in these situations is often critical, judgemental and forceful, using words like **must** and **should** or **have to**.\n\nThis voice we refer to as the inner critic.",
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "Continue",
            "conversationId": "EiRy9KYDgNt9MCxjoFsFfF-2"
          }
        ]
      }
    ]
  },
  "sv": {
    "id": "EiRy9KYDgNt9MCxjoFsFfF-1",
    "alias": "Self compassion - Inner critic 1",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Den inre kritikern",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "Den inre kritikern",
        "content": "Idag är det första steget på din resa mot ett mer konstruktivt och kärleksfullt förhållande till dig själv. I den här första lektionen får du en känsla av vad självmedkänsla är och hur det kan hjälpa dig i ditt liv.\n\n### Forskningen\n\nKursen är inspirerad av den evidensbaserade läroplanen Mindful Self-Compassion utvecklad av Kristen Neff, PhD och Cris Germer, PhD. Psykologerna vid 29k i samarbete med lärare och forskare från Center for Mindful Self-Compassion har skapat den här kursen.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Vad är den inre kritikern?",
        "title": "Vad är den inre kritikern?",
        "content": "I vardagen förväntar vi oss ofta att vara perfekta i flera roller samtidigt. Den fantastiska kompisen. Den kärleksfulla partnern. Den stora medarbetaren.\n\nSå småningom lägger vi bara inte till och då bedömer vi oss själva för att vi inte är bättre eller gör mer.\n\nSpråket vi använder mot oss själva i dessa situationer är ofta kritiskt, dömande och kraftfullt, med ord som * måste * och * borde * eller * måste *.\n\nDenna röst kallar vi den inre kritikern.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "video",
        "alias": "Upptäck din inre kritiker",
        "title": "Upptäck din inre kritiker",
        "description": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section4"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_video__letter_inner_friend-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622193763/Self%20Compassion/innercritic_exercise1_t222mc.mp4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "editorialWithImage",
        "alias": "Inre kritiker",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_writing_intro_a-min.jpg",
        "title": "Inre kritiker",
        "content": "De flesta människor märker aldrig sin värsta kritiker.\n\nDen rösten styr ofta våra liv. Håller oss tillbaka och skär oss från anslutningen.\n\nSå, låt oss börja med att titta inåt och hitta vår egen inre kritiker.\n",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section5"
          }
        ]
      },
      {
        "id": "section5",
        "type": "writing",
        "alias": "Hitta din inre kritiker",
        "title": "Hitta din inre kritiker",
        "time": "Ta 5-10 min",
        "question": "För följande övning, skriv ner ditt svar på ett papper och håll kvar det. Du kommer att behöva det senare.",
        "longDescription": "Hitta först en situation där du känner dig otillräcklig och skriv ner den så objektivt som möjligt, till exempel:\n\n> \"Jag känner mig otillräcklig när jag ser mig själv i spegeln på morgonen.\"\n\nSkriv sedan ner vad din inre kritiker säger. Försök att vara så specifik som möjligt, till exempel:\n\n> \"Du kommer sent. Du är alltid så jävla optimistisk. Jag är säker på att du kommer att göra dem besvikna. ”",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "audio",
        "title": "Meditation",
        "description": "Denna meditation kommer att förbereda dig för nästa steg i övningen.",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/yg_self_compassion/inner_critic/yogagirl_meditation_Lesson_01_Hearing_your_inner_voice.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section7"
          }
        ]
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Inre vän",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_writing_intro_b-min.jpg",
        "title": "Inre vän",
        "content": "Tänk först tillbaka på situationen som du skrev om tidigare.\n\nFöreställ dig nu hur du skulle svara på en vän i samma situation. Någon som känner till dina svagheter och styrkor, som älskar och inte bedömer. En vän som förstår din personliga historia och ser saker som är utom din kontroll.\n\nDin uppgift är att utforska vad din vän skulle säga till dig i samma situation.\n\n\n### De flesta vet inte att det finns\n\nTänk dig att du har en inre allierad och vän. Någon på din sida, på insidan. Någon som stöder dig och tror på dig villkorslöst.\n\nDetta nästa steg är en övning i att titta inåt och avslöja denna röst.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "writing",
        "alias": "Hitta din allierade och vän",
        "title": "Hitta din allierade och vän",
        "time": "Ta 5-10 min",
        "question": "Tänk tillbaka på samma känsla av otillräcklighet. Vad skulle en vän som älskar dig djupt säga till dig i den här situationen?",
        "longDescription": "Skriv ner vad en vän skulle säga. Försök att vara så specifik som möjligt, till exempel:\n\n\"Jag bryr mig djupt om dig och därför vill jag hjälpa dig att göra en förändring\"\n\n\"Jag älskar dig och jag vill inte att du ska lida\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section9"
          }
        ]
      },
      {
        "id": "section9",
        "type": "video",
        "alias": "Upptäck din inre vän",
        "title": "Upptäck din inre vän",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section10"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_video__letter_inner_friend-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621924376/Self%20Compassion/innercritic_exercise2_axzhqq.mp4"
          }
        ]
      },
      {
        "id": "section10",
        "type": "editorialWithImage",
        "alias": "# Bra ~~jobbat!~~",
        "title": "# Bra ~~jobbat!~~",
        "content": "Bra gjort när du har slutfört **inre kritiker** lektion, lära sig om:\n\n* Den inre kritikern\n* Den inre allierade\n\nNärhelst du märker att din inre kritiker dyker upp kan du också påminna dig själv om vad din inre vän skulle säga.",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Stäng"
          }
        ]
      }
    ]
  },
  "pt": {
    "id": "EiRy9KYDgNt9MCxjoFsFfF-1",
    "alias": "Self compassion - Inner critic 1",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Ajuda-nos a melhorar",
        "title": "Ajuda-nos a melhorar",
        "content": "O teu contributo é fundamental para **melhorarmos de forma contínua os nossos cursos**. Nesse sentido, a Escola de Medicina da Universidade do Minho em parceria com a FJN pretende avaliar o impacto dos cursos disponíveis na app 29K FJN.\n\nPara participar terás que clicar no link abaixo e responder a um questionário breve (**2 minutos**) no início e no final de cada curso. Este questionário é **confidencial**. \n\nDesde já obrigado pela tua ajuda. Não te esqueças de regressar aqui.\n\n[LINK](https://saudemental.p5.pt/profile/29k--fjn--emuminho?tt=FJjyv44HKrNhJkXozL6gQA)",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "A crítica ou o crítico interior ",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448851/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L1P1innercriticautocompaixao_lqwd9j.jpg",
        "title": "A crítica ou o crítico interior ",
        "content": "Hoje é o primeiro passo da tua caminhada rumo a uma relação mais construtiva e afetuosa contigo. Nesta primeira lição, terás uma ideia do que é a autocompaixão e de como te pode ajudar na tua vida.\n\n### A Investigação\n\nEste curso é inspirado no curso *Mindful Self-Compassion* desenvolvido pela Dra. Kristen Neff e pelo Dr. Cris Germer. Os psicólogos da 29k desenharam este curso em colaboração com professores e investigadores do *Center for Mindful Self-Compassion.*",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "O que é a crítica ou o crítico interior?",
        "title": "O que é a crítica ou o crítico interior?",
        "content": "No nosso dia-a-dia, queremos constantemente atingir a perfeição nos diversos papéis que desempenhamos. Queremos ser amigas, amigos perfeitos. Temos de ser companheiras, companheiros afetuosos e perfeitos. Como colegas de trabalho, queremos ser atentos, excelentes, perfeitos.\n\nAcabamos eventualmente por não conseguir fazer tudo e depois julgamo-nos por não sermos melhores ou por não fazermos mais.\n\nA linguagem que usamos para connosco nestas situações é, na maioria das vezes, crítica, contundente e cheia de julgamento, muitas vezes usamos palavras e expressões como \"*tens de fazer\",* \"*deves fazer\"* ou \"*tens de ter\"*.\n\nReferimo-nos a esta voz como a crítica ou o crítico interior.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "video",
        "alias": "Descobre a tua crítica ou o teu crítico interior",
        "title": "Descobre a tua crítica ou o teu crítico interior",
        "description": "“Tenho de ter esta capacidade para me encantar comigo, até nas minhas falhas.”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section5"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448851/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L1P3innercriticautocompaixao_scodk8.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622374550/29k%20FJN/Courses/Self%20Compassion/Videos%2020210530%20Update/FL_-_L1_Self_compassion_V1_iyomgc.mp4"
          }
        ]
      },
      {
        "id": "section5",
        "type": "editorialWithImage",
        "alias": "A crítica ou o crítico interior",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448852/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L1P4innercriticautocompaixao_br4xg7.jpg",
        "title": "A crítica ou o crítico interior",
        "content": "A maioria das pessoas nunca se apercebe da sua pior crítica ou do seu pior crítico.\n\nMuitas vezes, essa voz controla as nossas vidas. Impede-nos de avançar e corta a ligação connosco próprios.\n\nComecemos então por olhar para dentro de nós e por encontrar a nossa própria crítica ou crítico interior.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "writing",
        "alias": "Encontra a tua crítica ou crítico interior",
        "title": "Encontra a tua crítica ou crítico interior",
        "time": "Tira 5-10 min",
        "question": "Para o exercício seguinte, escreve a tua resposta numa folha de papel e guarda-a. Vais precisar dela mais tarde.",
        "longDescription": "Primeiro, pensa numa situação em que te sintas desajustada/o e descreve-a da forma mais objetiva possível. Por exemplo:\n\n> “Eu sinto-me desajustada/o quando me olho ao espelho de manhã”\n\nDepois escreve o que diz a tua crítica ou crítico interior. Tenta ser o mais específica/o possível, como por exemplo:\n\n> “Vais-te atrasar. És sempre demasiado otimista. Tenho a certeza de que os vais desiludir\"",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section7"
          }
        ]
      },
      {
        "id": "section7",
        "type": "audio",
        "title": "Meditação",
        "description": "Esta meditação vai preparar-te para o próximo passo do exercício.",
        "audioUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621523406/29k%20FJN/Meditations/Joanna%20Final/Hearing_Your_Inner_Voice_3m_jnxfu3.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section8"
          }
        ]
      },
      {
        "id": "section8",
        "type": "editorialWithImage",
        "alias": "A/o Amiga/o Interior",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448851/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L1P7innercriticautocompaixao_qa6us0.jpg",
        "title": "A/o Amiga/o Interior",
        "content": "Primeiro, pensa sobre a situação que descreveste anteriormente.\n\nAgora imagina como responderias a uma amiga ou a um amigo na mesma situação. Alguém que conhece bem as tuas fraquezas e os teus pontes fortes, que é amável e que não te julga. Uma amiga ou um amigo que compreende a tua história de vida e que vê as coisas que estão fora do teu controlo.\n\nA tua tarefa é explorar o que essa amiga ou esse amigo te diria na mesma situação.\n\n### A maioria das pessoas não sabe que ela/e existe\n\nImagina ter uma amiga ou amigo ou aliada/o interior. Alguém que está sempre contigo, ao teu lado, está no teu interior. Alguém que te apoia e acredita em ti incondicionalmente.\n\nEste próximo passo é um exercício para olhar para dentro e deixar sair esta voz.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section9"
          }
        ]
      },
      {
        "id": "section9",
        "type": "writing",
        "alias": "Encontrar a/o aliada/o e amiga/o",
        "title": "Encontrar a/o aliada/o e amiga/o",
        "time": "Tira 5-10 min",
        "question": "Pensa de novo em como é sentires-te desaquada/o. O que é que uma amiga ou amigo que te ama profundamente te diria nesta situação?",
        "longDescription": "Escreve o que uma amiga ou um amigo te diria. Tenta ser o mais específica/o possível, por exemplo:\n\n“Eu preocupo-me muito contigo e é por isso que gostaria de te ajudar a fazer uma mudança”\n\n“Eu gosto de ti e não quero que sofras”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section10"
          }
        ]
      },
      {
        "id": "section10",
        "type": "video",
        "alias": "Descobre a tua amiga ou amigo interior",
        "title": "Descobre a tua amiga ou amigo interior",
        "description": "“E nós somos a única companhia que temos até ao último suspiro.”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section11"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448852/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L1P9innercriticautocompaixao_zjbbah.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895789/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L1_Self_compassion_V2_why1q3.mp4"
          }
        ]
      },
      {
        "id": "section11",
        "type": "editorialWithImage",
        "alias": "# ~~Parabéns!~~",
        "title": "# ~~Parabéns!~~",
        "content": "Parabéns por completares a sessão **A** **Crítica Interior**, onde aprendeste mais sobre: \n\n* A/o crítica/o interior\n* A/o aliada/o interior\n\nQuando notares que a tua crítica ou o teu crítico interior a aparecer, podes lembrar-te do que diria a tua aliada ou o teu aliado interior.",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Fechar"
          }
        ]
      }
    ]
  }
}