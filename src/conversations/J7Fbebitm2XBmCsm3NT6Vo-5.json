{
  "en": {
    "id": "J7Fbebitm2XBmCsm3NT6Vo-5",
    "alias": "Self compassion - Body 5",
    "type": "lesson",
    "root": "section9",
    "sections": [
      {
        "id": "section9",
        "type": "editorialWithImage",
        "alias": "Self-soothing behaviors",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "",
        "content": "> \"Where the body goes the mind will follow\"\n>\n> \\- Unknown\n\n### The body can be of great help\n\nWe can’t escape stressful situations in life, no matter how much we try. They are a part of being human and something we all share.\n\nWhat we can do is to increase our ability to be self-compassionate during stressful events. Guide yourself towards this by helping your body to open up to a more self-compassionate way of acting.\n\nWe all work differently and have our own ways of comforting ourselves.\nLet’s explore yours.",
        "buttons": [
          {
            "component": "Button",
            "content": "Let's explore",
            "sectionId": "65293bee-f34b-4f66-a6dd-d907295f508c"
          }
        ],
        "infoItems": [
          {
            "title": "Self-soothing behaviors",
            "description": "The psychologist and Ph.D., Paul Gilbert calls it self-soothing behaviors. It can be lowering your shoulders. Taking a deep breath. Closing your eyes. Or maybe to tense your muscles for half a minute, and thereafter relax them."
          }
        ]
      },
      {
        "type": "editorialWithImage",
        "id": "65293bee-f34b-4f66-a6dd-d907295f508c",
        "content": "What research now is finding is that your body is deeply connected with your mind. As well as you mind deeply interconnected with your body.\n\nAll you have to do is give this an honest try and see for yourself. Put your hand in a fist. How does that make you feel? Now relax. Take a deep breath and place your hand kindly over your heart. Notice the difference?",
        "alias": "Why self-soothing works?",
        "title": "Why self-soothing works?",
        "infoItems": [
          {
            "title": "The role of oxytocin",
            "description": "Oxytocin is one of the most powerful neurotransmitters for feeling love and relaxation and is released when we feel safe, held and cared for. Like when a close friend comfortingly put their hand on you. The cool thing is that recent findings show that the emotional and limbic system in the brain actually is activated in the same way when you give yourself a similar physical gesture. Pretty good stuff."
          }
        ],
        "buttons": [
          {
            "component": "Button",
            "variant": "default",
            "content": "Continue",
            "sectionId": "section10"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "Take a deep breath and place your hand kindly over your heart.\n\n1. Think back to a challenging situation when you needed support. What gesture or self-soothing behavior could be helpful when you need it the most?\n2. Try it out.\n3. Play with this and see what feels best for you.",
        "buttons": [
          {
            "component": "NextConversationButton",
            "variant": "primary",
            "content": "I'm done",
            "conversationId": "9cc0b577-521f-4d60-8622-3c4b93a7a88b"
          }
        ],
        "question": "",
        "time": "Take 5 minutes",
        "alias": "Finding your gesture",
        "title": "Finding your gesture",
        "type": "writing",
        "id": "section10"
      }
    ]
  },
  "sv": {
    "id": "J7Fbebitm2XBmCsm3NT6Vo-5",
    "alias": "Self compassion - Body 5",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "video",
        "alias": "Om du kunde ändra en sak ...",
        "title": "Om du kunde ändra en sak ...",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section2"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/body/video-images/yg_body_video_intro-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622195268/Self%20Compassion/SC_body_intro_znogel.mp4"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Förkroppsligade känslor",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "Förkroppsligade känslor",
        "content": "### Trevligt att ha dig tillbaka.\n\nDenna lektion utforskar ett av de viktigaste verktygen för självmedkänsla - din kropp. Det är porten till både nuet, lära känna dig själv bättre och till och med ett kraftfullt verktyg för medkänsla.\n\n### Forskningen\n\nDen här lektionen är inspirerad av den bevisbaserade övningen ”Lugnande beröring” i MSC.\n\n### Dagens lektion består av fyra delar:\n\n* Lägg märke till känslor i svåra situationer (Skriva)\n* Din kropp; ditt hem (meditation)\n* Hjälpsamma kroppsgester (träning)\n\n### Förberedelse för lektionen:\n\n* 30 min ostörd tid\n* Penna och papper",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "Förkroppsligade känslor",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/shame/yg_shame_exercise_writing.jpg",
        "title": "Förkroppsligade känslor",
        "content": "Vi tar det för givet. Kanske för att det alltid finns där.\n\nVår kropp.\n\nOm vi märker det är det ofta att döma; Den är för stor. För liten. För smal. För fet. Eller det är för att driva det; Hårdare. Snabbare. Starkare.\n\nMen våra kroppar är också födelseplatsen för våra emotionella liv. Allt vi älskar och allt vi fruktar. Det känns här. Det är källan till oss och utgångspunkten för medkänsla.\n\nDen här lektionen handlar om att lära sig att märka stunder av lidande i din kropp och utforska hur det kan bli ett bra verktyg för självmedkänsla och en dörr till att leva i en djupare anslutning till dig själv.\n\nDu är inbjuden att se Yoga Girl arbeta genom ett kraftfullt ögonblick av stress och rädsla. Tillsammans med henne kan du utforska kroppens svar på en stressig händelse och de svåra känslor som dyker upp.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "video",
        "alias": "Rachel delar på\nförkroppsligande rädsla.",
        "title": "Rachel delar på\nförkroppsligande rädsla.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section5"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/body/video-images/yg_body_video_exercise1-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622195352/Self%20Compassion/SC_body_exercise_1_cyrsts.mp4"
          }
        ]
      },
      {
        "id": "section5",
        "type": "editorialWithImage",
        "alias": "Stressreaktionen",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/intention/yg_intention_intro-min.jpg",
        "title": "Stressreaktionen",
        "content": "Kroppen reagerar med oss och för oss.\n\nNär vi möter stressiga händelser förbereder våra kroppar sig för att hantera situationen. För att hjälpa oss på bästa sätt vet den hur.\n\nDen släpper ut energi och går in i lägena att antingen slåss, fly eller frysa. Allt för att skydda oss från vad de tror är externa hot.\n\nHur vi märker detta kan vara att vår hjärtfrekvens ökar, vi blir mer spända, får tunnelsyn och känner oss besvikna eller ibland till och med panik.\n\nOm våra kroppar reagerar på det här sättet betyder det faktiskt att de är friska och arbetar och försöker skydda oss från alla farliga situationer som våra forntida förfäder har överlevt.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "editorialWithImage",
        "alias": "",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/intention/yg_intention_intro-min.jpg",
        "title": "",
        "content": "När vi går igenom stressiga situationer är vi ofta så fångade i ögonblicket att vi inte märker vad som händer inom oss. Saknar möjligheten att försörja oss själva när vi behöver det mest.\n\nFör att bättre förstå och ta hand om dig själv utforskar den här övningen hur du kan märka och känna igen de ögonblick när du behöver medkänsla.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section7"
          }
        ]
      },
      {
        "shortDescription": "Skriv ner ditt svar på ett papper och håll kvar det, du behöver det senare.",
        "longDescription": "1. Tänk på en stressig eller svår situation från förra veckan. Det behöver inte vara den mest utmanande situationen, välj en som är uthärdlig för dig att komma ihåg just nu. Vad hände? Vem sa eller gjorde vad? Vad hände?\n2. Vilka fysiologiska förnimmelser upplevde du (till exempel spänningar, skakningar, skakningar, svettningar, domningar, illamående, tunnelsyn eller annat)?\n3. Vilken känsla kände du? Försök att välja den känsla som var mest framträdande (som nöd, rädsla, panik, irritation, sorg, ilska, skam eller annat).\n4. Försök att gå tillbaka till den situationen. Var i din kropp hittar du denna känsla, i bröstet, huvudet, magen eller annat?\n\n   Om du blir överväldigad nu av att komma ihåg situationen, kom ihåg att du alltid kan lämna den här övningen eller använda säkerhetsverktygssatsen i det övre högra hörnet.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section8"
          }
        ],
        "question": "",
        "time": "Ta tio minuter",
        "alias": "Titta djupare på dig själv",
        "title": "Titta djupare på dig själv",
        "type": "writing",
        "id": "section7"
      },
      {
        "id": "section8",
        "type": "video",
        "alias": "Känslor i kroppen",
        "title": "Känslor i kroppen",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section9"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/body/video-images/yg_body_video_exercise2-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622195439/Self%20Compassion/SC_body_exercise2_hmltdc.mp4"
          }
        ]
      },
      {
        "id": "section9",
        "type": "editorialWithImage",
        "alias": "Själv lugnande",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "Själv lugnande",
        "content": "### Kroppen kan vara till stor hjälp\n\nVi kan inte fly stressiga situationer i livet, oavsett hur mycket vi försöker. De är en del av att vara mänskliga och något vi alla delar.\n\nVad vi kan göra är att öka vår förmåga att vara självmedkännande under stressiga händelser. Väg dig själv mot detta genom att hjälpa din kropp att öppna sig för ett mer självmedkännande sätt att agera.\n\nPsykologen och doktorsexamen, Paul Gilbert, kallar det självreddande beteenden. Det kan sänka dina axlar. Andas djupt. Stänger ögonen. Eller kanske för att spänna dina muskler i en halv minut och sedan slappna av dem.\n\nVi arbetar alla olika och har våra egna sätt att trösta oss själva på.\nLåt oss utforska din.\n\n### Vetenskapen om varför fysiskt lugnande själv fungerar\n\nDet kan kännas kornigt eller dumt. Men det fungerar faktiskt.\n\nVad forskning nu finner är att din kropp är djupt kopplad till ditt sinne. Förutom att du tänker djupt sammankopplat med din kropp.\n\nAllt du behöver göra är att ge detta ett ärligt försök och se själv. Lägg handen i näven. Hur får du det att känna? Slappna av nu. Andas djupt och lägg din hand vänligt över ditt hjärta. Lägg märke till skillnaden?\n\nOxytocin och opioider är två av de mest kraftfulla hormonerna för att känna kärlek och avkoppling och frigörs när vi känner oss trygga, hållna och vårdade. Som när en nära vän tröstande lägger handen på dig. Det coola är att de senaste fynden visar att det emotionella och limbiska systemet i hjärnan faktiskt aktiveras på samma sätt när du ger dig själv en liknande fysisk gest. Ganska coola grejer.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section10"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "### Hitta din egen hjälpsamma och lugnande gest för tider av nöd\n\nTänk tillbaka på en utmanande situation när du behövde stöd. Vilken gest eller självreddande beteende kan vara till hjälp när du behöver det mest?\n\nTesta.\n\nLek med detta och se vad som känns bäst för dig. Skriv ner ditt nya verktyg för fysisk lugnande.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section11"
          }
        ],
        "question": "",
        "time": "Ta fem minuter",
        "alias": "Hitta din gest",
        "title": "Hitta din gest",
        "type": "writing",
        "id": "section10"
      },
      {
        "id": "section11",
        "type": "audio",
        "title": "Meditation",
        "description": "Denna meditation kommer att förbereda dig för nästa steg i övningen",
        "audioUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/audio/yg_self_compassion/body/yogagirl_meditation_Lesson_03_Body_and_Footing.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section12"
          }
        ]
      },
      {
        "id": "section12",
        "type": "video",
        "alias": "Reflektera över fysiska känslor",
        "title": "Reflektera över fysiska känslor",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section13"
          }
        ],
        "videos": [
          {
            "imageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/body/video-images/yg_body_video_outro-min.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1622195526/Self%20Compassion/SC_body_outro_duo9on.mp4"
          }
        ]
      },
      {
        "id": "section13",
        "type": "editorialWithImage",
        "alias": "Fortsätter övningen",
        "topImageUri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/yg_self_compassion/inner_critic/yg_inner_critic_intro-min.jpg",
        "title": "Fortsätter övningen",
        "content": "### Bra jobbat. Du har tagit stora steg på resan mot ett mer medkännande förhållande till dig själv och andra.\n\nVägen framåt är att fortsätta märka stunder av lidande i din kropp, känna igen dem för vad de är och hitta sätt att lugna dig själv.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section14"
          }
        ]
      },
      {
        "id": "section14",
        "type": "editorialWithImage",
        "alias": "# Bra ~~jobbat!~~",
        "title": "# Bra ~~jobbat!~~",
        "content": "Bra gjort när du har slutfört ** Body ** lektionen, du har precis gjort en underbar sak för dig själv.\n\nOm du väljer att delta i en delning med din grupp behöver du följande övningar nära till hands:\n\n* Titta djupare in i dig själv\n* Hitta din gest\n\n",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Stäng"
          }
        ]
      }
    ]
  },
  "pt": {
    "id": "J7Fbebitm2XBmCsm3NT6Vo-5",
    "alias": "Self compassion - Body 5",
    "type": "lesson",
    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "video",
        "alias": "Se pudesses mudar uma coisa...",
        "title": "Se pudesses mudar uma coisa...",
        "description": "“Comecei há uns anos a fazer um trabalho interior de aceitação cada vez maior do meu corpo e do meu rosto, porque comecei a preparar-me interiormente para envelhecer...”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section2"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448856/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4p1bodyautocompaixao_fohvlj.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895803/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L4_Self_compassion_V1_uia2xo.mp4"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Emoções incorporadas",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448856/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4p2bodyautocompaixao_aypggi.jpg",
        "title": "Emoções incorporadas",
        "content": "### É bom ter-te de volta.\n\nEsta sessão explora uma das ferramentas mais importante para a autocompaixão - o teu corpo. É a porta de entrada, quer para o momento presente, quer para te conheceres melhor, e uma ferramenta poderosa para a autocompaixão.\n\n### A investigação\n\nEsta sessão é inspirada na evidência do exercício do MSC \"O toque tranquilizador\".\n\n### A sessão de hoje tem 4 partes:\n\n* Dar conta de sensações em situações difíceis (Escrita)\n* O teu corpo, a tua casa (Meditação)\n* Gestos corporais úteis (Exercício)\n\n### Preparação para a sessão:\n\n* 30 min de tempo sem perturbação\n* Papel e caneta",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section3"
          }
        ]
      },
      {
        "id": "section3",
        "type": "editorialWithImage",
        "alias": "Emoções que se refletem no corpo",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448856/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4p3bodyautocompaixao_tr2ug6.jpg",
        "title": "Emoções que se refletem no corpo",
        "content": "Tomamo-lo como certo. Talvez porque está sempre lá.\n\nO nosso corpo.\n\nSe o notamos, é muitas vezes para o julgar: é demasiado grande, demasiado pequeno, demasiado magro, demasiado gordo. Ou é para o pressionar: mais esforço, mais rapidez, mais força.\n\nMas os nossos corpos são também o lugar de origem das nossas vidas emocionais. Tudo o que amamos e tudo o que tememos é sentido no nosso corpo. É a fonte da nossa existência e o ponto de partida para a autocompaixão.\n\nEsta sessão é sobre aprender a notar momentos de sofrimento no teu corpo e explorar como isto se pode tornar uma grande ferramenta para a autocompaixão e uma porta para viver uma ligação mais profunda contigo.\n\nEstás convidada/o para ver a Fátima a falar sobre um momento poderoso de stress e medo. Juntamente com ela, podes explorar a resposta do corpo a um evento stressante e as emoções difíceis que daí emergem.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section4"
          }
        ]
      },
      {
        "id": "section4",
        "type": "video",
        "alias": "A Fátima partilha medos que se refletem no corpo.",
        "title": "A Fátima partilha medos que se refletem no corpo.",
        "description": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section5"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448857/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P4bodyautocompaixao_rmj4lc.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895812/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L4_Self_compassion_V2_io8qrn.mp4"
          }
        ]
      },
      {
        "id": "section5",
        "type": "editorialWithImage",
        "alias": "A reação ao stress",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448857/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P5bodyautocompaixao_mfpeo6.jpg",
        "title": "A reação ao stress",
        "content": "O corpo reage connosco e para nós.\n\nQuando enfrentas um acontecimento stressante, o teu corpo prepara-se para lidar com a situação. Para te ajudar da melhor maneira que sabe.\n\nO corpo liberta energia e entra em modo de luta ou fuga ou fica bloqueado. Tudo para nos proteger do que ele pensa serem ameaças externas.\n\nPodemos dar-nos conta disto porque o nosso ritmo cardíaco aumenta, ficamos mais tensos, ficamos com visão em túnel ou sentimo-nos angustiados ou até em pânico.\n\nSe os nossos corpos reagem desta forma, isso significa que estão saudáveis e a funcionar corretamente, tentando proteger-nos de todas as situações perigosas a que os nossos antepassados sobreviveram.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ]
      },
      {
        "id": "section6",
        "type": "editorialWithImage",
        "alias": "Exercício",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448857/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P6bodyautocompaixao_e2j0v3.jpg",
        "title": "Exercício",
        "content": "Muitas vezes, quando passamos por situações stressantes, ficamos tão envolvidos no momento que não nos damos conta do que está a acontecer dentro de nós. Perdemos a oportunidade de nos apoiarmos quando mais precisamos.\n\nPara te compreenderes melhor e cuidares de ti própria/o, este exercício explora como podes dar conta e reconhecer os momentos em que necessitas de autocompaixão.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section7"
          }
        ]
      },
      {
        "shortDescription": "Escreve a tua resposta numa folha de papel e guarda-a, vais precisar dela mais tarde.",
        "longDescription": "1. Pensa numa situação stressante ou difícil da semana passada. Não tem de ser a situação mais desafiante, escolhe uma que seja suportável para te recordares agora mesmo. O que aconteceu? Quem disse ou fez o quê? O que se estava a passar?\n2. Que sensações fisiológicas tiveste (por exemplo, tensão, tremores, arrepios, suores, dormência, náuseas, visão em túnel ou outra)? \n3. Que emoção sentiste? Tenta escolher a emoção mais marcante (como angústia, medo, pânico, irritação, tristeza, raiva, vergonha ou outra).\n4. Tenta voltar a essa situação. Em que parte do teu corpo localizas essa emoção? No peito, na cabeça, no estômago ou noutra zona?\n\n   Se te sentires assoberbada/o ao recordar a situação, lembra-te que podes sempre sair deste exercício ou usar o kit de ferramentas de segurança disponível no canto superior direito.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section8"
          }
        ],
        "question": "",
        "time": "Tira 10 min",
        "alias": "Olha profundamente para dentro de ti",
        "title": "Olha profundamente para dentro de ti",
        "type": "writing",
        "id": "section7"
      },
      {
        "id": "section8",
        "type": "video",
        "alias": "Emoções no corpo",
        "title": "Emoções no corpo",
        "description": "“O nosso corpo é transparente. Ele diz-nos tudo sobre nós, ele diz-nos tudo.”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section9"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448858/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P8bodyautocompaixao_maxwen.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895816/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L4_Self_compassion_V3_qowa3n.mp4"
          }
        ]
      },
      {
        "id": "section9",
        "type": "editorialWithImage",
        "alias": "Autotranquilizante",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448858/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P9bodyautocompaixao_eijlnj.jpg",
        "title": "Autotranquilizante",
        "content": "### O corpo pode ser uma grande ajuda\n\nNão conseguimos escapar às situações stressantes da vida, por muito que tentemos. Fazem parte de ser humano e são comuns a todos nós.\n\nO que podemos fazer é aumentar a nossa capacidade de sermos autocompassivos durante acontecimentos stressantes. Adota esta atitude, ajudando o teu corpo a abrir-se para uma forma de agir mais autocompassiva.\n\nO psicólogo Dr. Paul Gilbert dá-lhe o nome de comportamentos autotranquilizantes, que passam por relaxar os ombros, respirar fundo, fechar os olhos ou contrair os músculos durante meio minuto e depois relaxá-los.\n\nTodos nós somos diferentes e temos as nossas próprias formas de nos confortarmos.\nVamos explorar as tuas.\n\n### A ciência explica a necessidade de te acalmares fisicamente\n\nPode parecer patético ou estúpido, mas funciona.\n\nOs resultados científicos mostram que o teu corpo está profundamente ligado à tua mente. Tal como a tua mente está profundamente interligada com o teu corpo.\n\nTudo o que tens de fazer é experimentar e ver com os teus próprios olhos. Fecha a mão com força. Como é que isso te faz sentir? Agora relaxa. Respira fundo e coloca suavemente a tua mão sobre o coração. Notas a diferença?\n\nA oxitocina é um dos neurotransmissores mais poderosos para sentirmos amor e relaxamento e é libertada quando nos sentimos seguros, amparados e acarinhados. Como quando uma amiga ou um amigo próximo te dá um abraço. O mais interessante é que descobertas recentes mostram que os sistemas emocional e límbico do cérebro são ativados da mesma forma quando tens para contigo um gesto físico semelhante. Muito boas notícias!",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section10"
          }
        ]
      },
      {
        "shortDescription": "",
        "longDescription": "### Encontra o teu gesto autotranquilizante para os momentos de necessidade\n\nLembra-te de uma situação desafiante em que precisaste de apoio. Que gesto ou comportamento tranquilizador poderia ser útil nesta situação?\n\nExperimenta-o.\n\nExperimenta outros gestos e verifica qual te faz sentir melhor. Descreve a tua nova técnica para te acalmares fisicamente.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section11"
          }
        ],
        "question": "",
        "time": "Tira 5 min",
        "alias": "Encontra o teu gesto",
        "title": "Encontra o teu gesto",
        "type": "writing",
        "id": "section10"
      },
      {
        "id": "section11",
        "type": "audio",
        "title": "Meditação",
        "description": "Esta meditação vai preparar-te para o próximo passo do exercício.",
        "audioUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621523413/29k%20FJN/Meditations/Joanna%20Final/Body_And_Footing_rsqkwh.mp3",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section12"
          }
        ]
      },
      {
        "id": "section12",
        "type": "video",
        "alias": "Refletir sobre as emoções físicas",
        "title": "Refletir sobre as emoções físicas",
        "description": "“Não é que eu tivesse saído de fábrica com este kit, (…) mas andei à procura dele (…) e encontrei aquilo que funciona comigo...”",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section13"
          }
        ],
        "videos": [
          {
            "imageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P12bodyautocompaixao_y35wsm.jpg",
            "videoUri": "https://res.cloudinary.com/twentyninek/video/upload/q_auto,t_global/v1621895795/29k%20FJN/Courses/Self%20Compassion/Videos/FL_-_L4_Self_compassion_V4_g15c3f.mp4"
          }
        ]
      },
      {
        "id": "section13",
        "type": "editorialWithImage",
        "alias": "Continua a praticar",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/q_auto,t_global/v1621448859/29k%20FJN/Courses/Self%20Compassion/Photos%20Final%20(Use%20This%20One)/L4P13bodyautocompaixao_fbnddt.jpg",
        "title": "Continua a praticar",
        "content": "### Excelente trabalho.\n\nDeste grandes passos na caminhada rumo a uma relação mais compassiva contigo e com os outros.\n\nO caminho a seguir é continuar a notar os momentos de sofrimento no teu corpo, reconhecendo-os pelo que eles são e encontrando formas de te tranquilizares.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section14"
          }
        ]
      },
      {
        "id": "section14",
        "type": "editorialWithImage",
        "alias": "# ~~Parabéns!~~",
        "title": "# ~~Parabéns!~~",
        "content": "Parabéns por completares a lição **Corpo**, fizeste uma coisa maravilhosa pelo teu bem-estar.\n\nSe decidires participar numa partilha com o teu grupo, vais precisar de ter à mão estes exercícios:\n\n* Olha profundamente para dentro de ti\n* Encontra o teu gesto",
        "buttons": [
          {
            "component": "LessonCompleteButton",
            "content": "Fechar"
          }
        ]
      }
    ]
  }
}