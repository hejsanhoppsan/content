{
  "en": {
    "id": "2f046825-223d-4ace-8015-2cfeebe3eb22",

    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Act for Change",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Act_for_change_uz8ott.png",
        "title": "Act for Change",
        "content": "*20 minutes*\n\nA problem properly defined is a problem half solved. In this exercise, you’ll explore one thing that is currently contributing to your anxiety and identify what can be done about it; from brainstorming solutions to planning concrete steps.",
        "buttons": [
          {
            "component": "Button",
            "content": "Let’s do this",
            "sectionId": "section2"
          }
        ],
        "infoItems": []
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Define your triggers",
        "title": "Define your triggers",
        "content": "When it comes to defining anxiety triggers, examples like “I worry about the world” are vague and abstract. A more accurate example like “I worry about losing my income if the economy really crashes” helps us better identify new solutions like “make sure I have a valid unemployment insurance” or “apply for a second job”. With that in mind, let’s move on to defining one aspect of this crisis that is causing you anxiety right now.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section3"
          }
        ]
      },
      {
        "shortDescription": "Write down your answer on a piece of paper and hold on to it, you’ll need it later.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section4"
          }
        ],
        "question": "As precisely as possible, define a problem that’s contributing to your anxiety during the current crisis. Make sure you pick something that is not too overwhelming and that you realistically can solve within a week.",
        "time": "Take 5-10 min",
        "alias": "Step 1",
        "title": "Step 1",
        "type": "writing",
        "id": "section3"
      },
      {
        "shortDescription": "Write down your ideas on a piece of paper and hold on to it, you’ll need it later.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section5"
          }
        ],
        "question": "Now take a moment to consider ALL possible solutions that come to mind. Yes there’s even a room for wild and ridiculous solutions, so don’t hold back.",
        "time": "Take 5-10 min",
        "alias": "Step 2",
        "title": "Step 2",
        "type": "writing",
        "id": "section4"
      },
      {
        "shortDescription": "Write down your scores next to the ideas and hold on to the paper, you’ll be needing it later.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "section6"
          }
        ],
        "question": "Give each solution a score from 0 to 10, depending on how helpful you think it would be in solving your problem.",
        "time": "Take 5-10 min",
        "alias": "Step 3",
        "title": "Step 3",
        "type": "writing",
        "id": "section5"
      },
      {
        "shortDescription": "",
        "longDescription": "Based on your notes, pick one solution and a time when you can implement it in the coming week. Make sure your goal is achievable and measurable so that you know when you’ve reached it.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continue",
            "sectionId": "7a1ca046-4d81-42d8-b645-cc58611c7302"
          }
        ],
        "question": "Pick a solution and a time when you can implement it in the coming week. Make sure your goal is achievable and measurable so that you know when you’ve reached it.",
        "time": "Take 5-10 min",
        "alias": "Step 4",
        "title": "Step 4",
        "type": "writing",
        "id": "section6"
      },
      {
        "type": "sharingExercise",
        "previewSharings": true,
        "enableEarlySkip": true,
        "id": "7a1ca046-4d81-42d8-b645-cc58611c7302",
        "alias": "Sharing wall: act for change",
        "exampleText": "I might be more relaxed on a daily basis and open to new challenges instead of avoiding them.",
        "question": "How would your life look if you applied this method to other challenges you have?",
        "nextSection": "section7"
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Well done!",
        "topImageUri": "",
        "title": "Well done!",
        "content": "Well done for your willingness to act for change.\n\nOnce you’ve had a chance to implement your solution, consider:\n\n* Are you satisfied with how it went?\n* If yes, which area of your life has improved as a result?\n* If not, can you give it another go or try another solution?",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Finish"
          }
        ]
      }
    ],
    "alias": "Exercise: Act for change"
  },
  "sv": {
    "id": "2f046825-223d-4ace-8015-2cfeebe3eb22",

    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Lag för förändring",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Act_for_change_uz8ott.png",
        "title": "Lag för förändring",
        "content": "*20 minuter*\n\nEtt korrekt definierat problem är ett halvt löst problem. I den här övningen kommer du att utforska en sak som för närvarande bidrar till din ångest och identifiera vad som kan göras åt det; från idékläckning till planering av konkreta steg.",
        "buttons": [
          {
            "component": "Button",
            "content": "Nu kör vi",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Definiera dina triggers",
        "title": "Definiera dina triggers",
        "content": "När det gäller att definiera ångestutlösare är exempel som \"Jag oroar mig för världen\" vaga och abstrakta. Ett mer exakt exempel som \"Jag oroar mig för att förlora min inkomst om ekonomin verkligen kraschar\" hjälper oss att bättre identifiera nya lösningar som \"se till att jag har en giltig arbetslöshetsförsäkring\" eller \"ansöka om ett andra jobb\". Med det i åtanke, går vi vidare till att definiera en aspekt av denna kris som orsakar dig ångest just nu.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section3"
          }
        ]
      },
      {
        "shortDescription": "Skriv ner ditt svar på ett papper och håll kvar det, du behöver det senare.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section4"
          }
        ],
        "question": "Så exakt som möjligt, definiera ett problem som bidrar till din ångest under den aktuella krisen. Se till att du väljer något som inte är för överväldigande och som du realistiskt kan lösa inom en vecka.",
        "time": "Ta 5-10 min",
        "alias": "Steg 1",
        "title": "Steg 1",
        "type": "writing",
        "id": "section3"
      },
      {
        "shortDescription": "Skriv ner dina idéer på ett papper och håll fast vid det, du behöver dem senare.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section5"
          }
        ],
        "question": "Ta nu en stund att överväga ALLA möjliga lösningar som kommer att tänka på. Ja, det finns till och med ett utrymme för vilda och löjliga lösningar, så håll inte tillbaka.",
        "time": "Ta 5-10 min",
        "alias": "Steg 2",
        "title": "Steg 2",
        "type": "writing",
        "id": "section4"
      },
      {
        "shortDescription": "Skriv ner dina poäng bredvid idéerna och håll fast på tidningen, du behöver det senare.",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section6"
          }
        ],
        "question": "Ge varje lösning poäng från 0 till 10, beroende på hur bra du tror att det skulle vara att lösa ditt problem.",
        "time": "Ta 5-10 min",
        "alias": "Steg 3",
        "title": "Steg 3",
        "type": "writing",
        "id": "section5"
      },
      {
        "shortDescription": "",
        "longDescription": "Baserat på dina anteckningar, välj en lösning och en tid när du kan implementera den under den kommande veckan. Se till att ditt mål är uppnåbart och mätbart så att du vet när du når det.",
        "buttons": [
          {
            "component": "Button",
            "content": "Fortsätta",
            "sectionId": "section7"
          }
        ],
        "question": "Välj en lösning och en tid då du kan implementera den under den kommande veckan. Se till att ditt mål är uppnåbart och mätbart så att du vet när du når det.",
        "time": "Ta 5-10 min",
        "alias": "Steg 4",
        "title": "Steg 4",
        "type": "writing",
        "id": "section6"
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Bra gjort!",
        "topImageUri": "",
        "title": "Bra gjort!",
        "content": "Bra gjort för din vilja att agera för förändring.\n\nNär du har haft en chans att implementera din lösning kan du överväga:\n\n* Är du nöjd med hur det gick?\n* Om ja, vilket område i ditt liv har förbättrats som ett resultat?\n* Om inte, kan du ge det en gång till eller prova en annan lösning?",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Avsluta"
          }
        ]
      }
    ],
    "alias": "Exercise: Act for change"
  },
  "pt": {
    "id": "2f046825-223d-4ace-8015-2cfeebe3eb22",

    "root": "section1",
    "sections": [
      {
        "id": "section1",
        "type": "editorialWithImage",
        "alias": "Agir para mudar",
        "topImageUri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1620653687/Illustrations_Exercises/Heading_Act_for_change_uz8ott.png",
        "title": "Agir para mudar",
        "content": "*20 minutos*\n\nUm problema devidamente definido é um problema meio resolvido. Neste exercício, vais explorar algo que está a contribuir para a tua ansiedade neste momento e identificar o que pode ser feito a esse respeito, desde o debate de ideias até ao planeamento de passos concretos.",
        "buttons": [
          {
            "component": "Button",
            "content": "Vamos a isto",
            "sectionId": "section2"
          }
        ]
      },
      {
        "id": "section2",
        "type": "editorialWithImage",
        "alias": "Define os teus gatilhos ",
        "title": "Define os teus gatilhos ",
        "content": "Quando se trata de definir os gatilhos de ansiedade, exemplos como \"preocupo-me com o mundo\" são vagos e abstratos. Um exemplo mais concreto como \"preocupo-me em perder o meu rendimento se a economia colapsar\" ajuda-nos a identificar melhor soluções novas como \"certificar-me de que tenho direito ao subsídio de desemprego\" ou \"candidatar-me a um segundo emprego\". Tendo isto em consideração, vamos passar à definição de um aspeto da crise que te está a causar ansiedade neste momento.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section3"
          }
        ]
      },
      {
        "shortDescription": "Escreve a tua resposta num pedaço de papel e guarda-o, Vais precisar dele mais tarde. ",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section4"
          }
        ],
        "question": "Define o mais concretamente possível um problema que está a contribuir para a tua ansiedade durante a crise atual. Certifica-te de que escolhes algo que não seja demasiado avassalador e que possas resolver realisticamente no espaço de uma semana.",
        "time": "Tempo: 5-10 min",
        "alias": "Passo 1",
        "title": "Passo 1",
        "type": "writing",
        "id": "section3"
      },
      {
        "shortDescription": "Escreve as tuas ideias num pedaço de papel e guarda-o, vais precisar dele mais tarde. ",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section5"
          }
        ],
        "question": "Agora dedica um momento a considerar TODAS as soluções possíveis que te vierem à cabeça. Sim! Até as soluções extravagantes e ridículas, por isso não te retraias. ",
        "time": "Tempo: 5-10 min",
        "alias": "Passo 2",
        "title": "Passo 2",
        "type": "writing",
        "id": "section4"
      },
      {
        "shortDescription": "Escreve a tua pontuação ao lado das ideias e guarda o papel, vais precisar dele mais tarde. ",
        "longDescription": "",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "section6"
          }
        ],
        "question": "Dá uma pontuação de 0 a 10 a cada solução, dependendo de quão útil achas que seria na resolução do teu problema. ",
        "time": "Tempo: 5-10 min",
        "alias": "Passo 3",
        "title": "Passo 3",
        "type": "writing",
        "id": "section5"
      },
      {
        "shortDescription": "",
        "longDescription": "Certifica-te de que o teu objetivo é alcançável e mensurável para que saibas quando o atingiste.",
        "buttons": [
          {
            "component": "Button",
            "content": "Continuar",
            "sectionId": "8f257682-c8b3-4be9-ba73-803f66482cd5"
          }
        ],
        "question": "Com base nas tuas notas, escolhe uma solução e uma altura na próxima semana em que a possas implementar.",
        "time": "Tempo: 5-10 min",
        "alias": "Passo 4",
        "title": "Passo 4",
        "type": "writing",
        "id": "section6"
      },
      {
        "type": "sharingExercise",
        "previewSharings": true,
        "enableEarlySkip": true,
        "id": "8f257682-c8b3-4be9-ba73-803f66482cd5",
        "alias": "Sharing wall: act for change",
        "question": "Como seria a tua vida se aplicasses este método a alguns dos teus outros desafios?",
        "exampleText": "Provavelmente terias dias mais relaxados e estaria aberta/o a novos desafios em vez de os evitar",
        "nextSection": "section7"
      },
      {
        "id": "section7",
        "type": "editorialWithImage",
        "alias": "Parabéns!",
        "topImageUri": "",
        "title": "Parabéns!",
        "content": "Parabéns pela tua determinação em agir para mudar! \n\nAssim que tiveres a oportunidade de implementar a tua solução, considera estes pontos: \n\n* Estás satisfeita/o com a forma como correu? \n* Se sim, que área da tua vida melhorou em virtude desta solução? \n* Se não, podes tentar de novo ou experimentar outra solução?",
        "buttons": [
          {
            "component": "ExerciseCompleteButton",
            "content": "Terminar"
          }
        ]
      }
    ],
    "alias": "Exercise: Act for change"
  }
}
