{
  "en": {
    "id": "relationships-thingsgowrong-1",
    "alias": "YA building relationships 3: thingsgowrong-1",
    "root": "1",
    "sections": [
      {
        "content": [
          {
            "component": "HeaderImage",
            "content": {
              "uri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1619779806/Building%20Relationships/when-things-go-wrong_header_hco9x7.png"
            }
          },
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "# When things go wrong\n\nIt can feel easy to handle relationships when everything is going well. But sometimes we make mistakes. In this lesson you will learn to increase the chance that you behave as **you** want."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Keep going",
                "sectionId": "5f266d48-3e74-41f7-91f5-7acf6bb6850f"
              }
            ]
          }
        ],
        "id": "1",
        "alias": "1",
        "type": "defaultSection"
      },
      {
        "type": "editorialWithImage",
        "id": "5f266d48-3e74-41f7-91f5-7acf6bb6850f",
        "alias": "1b",
        "content": "In this lesson you'll learn about:\n\n* When things go wrong\n* Aware and allow in hard situations\n* Difficult thoughts\n* Practice",
        "buttons": [
          {
            "component": "Button",
            "content": "Let's go",
            "sectionId": "2"
          }
        ]
      },
      {
        "id": "2",
        "alias": "2",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Do you remember the breathing pause? And the **aware** and **allow** skills we've been talking about? They help you create relationships based on what is important to you.\n\nToday you should try to be aware and allowing even in  **difficult**  situations."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Okey",
                "sectionId": "3"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "3",
        "alias": "3",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Being aware and allowing of difficult thoughts and feelings is extra important (and difficult) when we socialise with others.\n\nAt the same time, it can help you do more of what you want to do in a nice way. But in many situations things happen quickly and it's easy to mess up."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Got it",
                "sectionId": "4"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "4",
        "alias": "4",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Being **aware** helps you to be the way you want when you're with others. Being **allowing** helps you accept everything that happens inside you.\n\nTogether they give you more **control** so you can do what is **important** to you."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "5"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "5",
        "alias": "5",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "But things don't always turn out the way we want. What do you usually do when you react impulsively? \n\nWrite down your answer where only you can see it, like this:\n\n\"When I react impulsively, I usually ...\""
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "6"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "6",
        "alias": "6",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Good job. Now you know what you usually do when things go wrong.\n\nBeing allowing towards what happens on the inside often means that we become better at handling situations in a way that we can feel satisfied with afterwards."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "More",
                "sectionId": "7"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "7",
        "alias": "7",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Sometimes we confuse \"allowing\" with \"passive.\" But allowing is not the same as not reacting. And it's not letting others behave in just any way toward us either.\n\nTo allow is to let things on the **inside** be as they are, without trying to control, change or judge them."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "NextConversationButton",
                "content": "Got it",
                "conversationId": "relationships-thingsgowrong-2",
                "variant": "primary"
              }
            ]
          }
        ],
        "type": "defaultSection"
      }
    ]
  },
  "sv": {
    "id": "relationships-thingsgowrong-1",
    "alias": "YA building relationships 3: thingsgowrong-1",
    "root": "1a",
    "sections": [
      {
        "type": "defaultSection",
        "id": "1a",
        "alias": "1a",
        "content": [
          {
            "component": "HeaderImage",
            "content": {
              "uri": "https://storage.googleapis.com/k-org-879a8.appspot.com/images/courses/fe91a529-e05c-4baf-97ab-336ed8f8c8bb/tacksamhet_header.png"
            }
          },
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "# När det blir fel\n\nDet kan kännas lätt att hantera relationer när allt flyter på fint. Men ibland gör vi misstag. I den här utmaningen får du lära dig att öka chansen att du beter dig som **du** vill."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Fortsätt",
                "sectionId": "1"
              }
            ]
          }
        ]
      },
      {
        "id": "1",
        "alias": "1",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "I den här lektionen lär du dig om:\n\n* När det blir fel\n* Medveten och tillåtande i svåra situationer\n* Jobbiga tankar\n* Öva"
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Kör igång",
                "sectionId": "2"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "2",
        "alias": "2",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Kommer du ihåg andningspausen? Och färdigheterna **medveten** och **tillåtande** som vi har pratat om? De hjälper dig att skapa relationer som utgår från vad som är viktigt för dig.\n\nIdag ska du få prova att vara **medveten** och **tillåtande** även i **svåra** situationer."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Okej",
                "sectionId": "3"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "3",
        "alias": "3",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Att vara medveten om och tillåta jobbiga tankar och känslor är extra viktigt (och svårt) när vi umgås med andra. \n\nSamtidigt kan det hjälpa dig att göra mer av det du vill göra på ett schysst sätt. Men i många situationer går det fort och det är lätt hänt att vi ställer till det."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Fortsätt",
                "sectionId": "4"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "4",
        "alias": "4",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Att vara **medveten** hjälper dig att vara med andra på det sätt som du vill. Att vara **tillåtande** hjälper dig acceptera allt som händer inuti dig. \n\nTillsammans ger de dig mer **kontroll** så du kan göra det som är **viktigt** för dig."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Nästa",
                "sectionId": "5"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "5",
        "alias": "5",
        "content": [
          {
            "component": "Exercise",
            "content": [
              {
                "component": "TextGroup",
                "content": [
                  {
                    "component": "Text",
                    "content": "Men det är ju inte alltid saker blir som vi vill. Vad brukar du göra när du reagerar impulsivt? Skriv ned ditt svar där bara du kan se det, så här:\n\n”När jag reagerar impulsivt brukar jag...”"
                  }
                ]
              }
            ],
            "exerciseId": "relationships-things-go-wrong",
            "exerciseTitle": "När det blir fel"
          },
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Guiding",
                "content": "Exempel: När jag reagerar impulsivt brukar jag säga saker jag egentligen inte menar."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Jag är klar",
                "sectionId": "6"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "6",
        "alias": "6",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Bra jobbat. Nu vet du vad du brukar göra när det blir fel."
              },
              {
                "component": "Text",
                "content": "Att vara tillåtande mot det som händer på insidan innebär ofta att vi blir bättre på att hantera situationer på ett sätt som vi kan känna oss nöjda med efteråt."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Fortsätt",
                "sectionId": "7"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "7",
        "alias": "7",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Ibland blandar vi ihop ”tillåtande” med ”passiv”. Men att tillåta är inte samma sak som att inte reagera. Det är inte heller att låta andra bete sig hur som helst mot oss. \n\nAtt tillåta är att låta saker på **insidan** vara som de är, utan att försöka kontrollera, ändra eller döma dem."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "NextConversationButton",
                "content": "Gå vidare",
                "conversationId": "relationships-thingsgowrong-2",
                "variant": "primary"
              }
            ]
          }
        ],
        "type": "defaultSection"
      }
    ]
  },
  "pt": {
    "id": "relationships-thingsgowrong-1",
    "alias": "YA building relationships 3: thingsgowrong-1",
    "root": "1",
    "sections": [
      {
        "content": [
          {
            "component": "HeaderImage",
            "content": {
              "uri": "https://res.cloudinary.com/twentyninek/image/upload/f_auto,q_auto,t_global/v1619779806/Building%20Relationships/when-things-go-wrong_header_hco9x7.png"
            }
          },
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "# When things go wrong\n\nIt can feel easy to handle relationships when everything is going well. But sometimes we make mistakes. In this lesson you will learn to increase the chance that you behave as **you** want."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Keep going",
                "sectionId": "5f266d48-3e74-41f7-91f5-7acf6bb6850f"
              }
            ]
          }
        ],
        "id": "1",
        "alias": "1",
        "type": "defaultSection"
      },
      {
        "type": "editorialWithImage",
        "id": "5f266d48-3e74-41f7-91f5-7acf6bb6850f",
        "alias": "1b",
        "content": "In this lesson you'll learn about:\n\n* When things go wrong\n* Aware and allow in hard situations\n* Difficult thoughts\n* Practice",
        "buttons": [
          {
            "component": "Button",
            "content": "Let's go",
            "sectionId": "2"
          }
        ]
      },
      {
        "id": "2",
        "alias": "2",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Do you remember the breathing pause? And the **aware** and **allow** skills we've been talking about? They help you create relationships based on what is important to you.\n\nToday you should try to be aware and allowing even in  **difficult**  situations."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Okey",
                "sectionId": "3"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "3",
        "alias": "3",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Being aware and allowing of difficult thoughts and feelings is extra important (and difficult) when we socialise with others.\n\nAt the same time, it can help you do more of what you want to do in a nice way. But in many situations things happen quickly and it's easy to mess up."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Got it",
                "sectionId": "4"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "4",
        "alias": "4",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Being **aware** helps you to be the way you want when you're with others. Being **allowing** helps you accept everything that happens inside you.\n\nTogether they give you more **control** so you can do what is **important** to you."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "5"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "5",
        "alias": "5",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "But things don't always turn out the way we want. What do you usually do when you react impulsively? \n\nWrite down your answer where only you can see it, like this:\n\n\"When I react impulsively, I usually ...\""
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "Next",
                "sectionId": "6"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "6",
        "alias": "6",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Good job. Now you know what you usually do when things go wrong.\n\nBeing allowing towards what happens on the inside often means that we become better at handling situations in a way that we can feel satisfied with afterwards."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "Button",
                "content": "More",
                "sectionId": "7"
              }
            ]
          }
        ],
        "type": "defaultSection"
      },
      {
        "id": "7",
        "alias": "7",
        "content": [
          {
            "component": "TextGroup",
            "content": [
              {
                "component": "Text",
                "content": "Sometimes we confuse \"allowing\" with \"passive.\" But allowing is not the same as not reacting. And it's not letting others behave in just any way toward us either.\n\nTo allow is to let things on the **inside** be as they are, without trying to control, change or judge them."
              }
            ]
          },
          {
            "component": "ButtonGroup",
            "content": [
              {
                "component": "NextConversationButton",
                "content": "Got it",
                "conversationId": "relationships-thingsgowrong-2",
                "variant": "primary"
              }
            ]
          }
        ],
        "type": "defaultSection"
      }
    ]
  }
}
