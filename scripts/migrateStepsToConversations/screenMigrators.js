const Button = (content, nextSectionId) =>
  nextSectionId === 'END_LESSON'
    ? {
        component: 'LessonCompleteButton',
        content,
      }
    : nextSectionId === 'END_EXERCISE'
    ? {
        component: 'ExerciseCompleteButton',
        content,
      }
    : {
        component: 'Button',
        content: content,
        sectionId: nextSectionId,
      };

const editorialWithImage = (
  type,
  { topImageUri, title, content, buttonText },
  conversationId,
  sectionId,
  nextSectionId,
) => ({
  id: sectionId,
  type,
  alias: title,
  topImageUri,
  title,
  content,
  buttons: [Button(buttonText, nextSectionId)],
});

const writing = (
  type,
  { title, time, question, shortDescription, longDescription, buttonText },
  conversationId,
  sectionId,
  nextSectionId,
) => ({
  id: sectionId,
  type,
  alias: title,
  title,
  time,
  question,
  shortDescription,
  longDescription,
  buttons: [Button(buttonText, nextSectionId)],
});

const audio = (
  type,
  { title, description, audioUri, buttonText },
  conversationId,
  sectionId,
  nextSectionId,
) => ({
  id: sectionId,
  type,
  title,
  description,
  audioUri,
  buttons: [Button(buttonText, nextSectionId)],
});

const video = (
  type,
  { title, description, videoUri, imageUri, buttonText },
  conversationId,
  sectionId,
  nextSectionId,
) => ({
  id: sectionId,
  type,
  alias: title,
  title,
  description,
  videoUri,
  imageUri,
  buttons: [Button(buttonText, nextSectionId)],
});

export { editorialWithImage, writing, audio, video };
