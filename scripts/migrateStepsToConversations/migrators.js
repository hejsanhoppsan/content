import fs from 'fs';
import path from 'path';

import createRequire from '../../lib/require.cjs';

import { createConversationIdFromLesson } from '../../lib/id.js';
/*eslint import/namespace: ['error', { allowComputed: true }]*/
import * as screenMigrators from './screenMigrators.js';

const cjsRequire = createRequire(import.meta.url);
const lessonCompletedScreen = cjsRequire(
  '../../src/ui/Screen.LessonCompleted.json',
);

const getConversationFilePath = (conversationId) =>
  path.resolve(__dirname, `../../src/conversations/${conversationId}.json`);

const readConversationFile = (conversationId) =>
  fs.existsSync(getConversationFilePath(conversationId)) &&
  JSON.parse(fs.readFileSync(getConversationFilePath(conversationId)));

const writeConversationFile = (conversationId, data) =>
  fs.writeFileSync(
    getConversationFilePath(conversationId),
    JSON.stringify(data, null, 2),
  );

const migrateStep = (conversationId, isLesson) => (step, index, steps) => {
  const { screenType } = step;

  if (!(screenType in screenMigrators)) {
    console.error(
      `There is no migrator defined for type ${screenType} in ${conversationId}`,
    );
    process.exit(1);
  }

  const migrator = screenMigrators[screenType];

  const sectionId = `section${index + 1}`;
  const nextSectionId =
    index < steps.length - 1
      ? `section${index + 2}`
      : isLesson
      ? 'END_LESSON'
      : 'END_EXERCISE';

  return migrator(screenType, step, conversationId, sectionId, nextSectionId);
};

const migrateSteps = (
  language,
  conversationId,
  conversationAlias,
  data,
  isLesson = false,
) => {
  if ('conversations' in data) {
    console.error(
      `There are already conversations defined for ${language} in "${conversationId}"`,
    );
    process.exit(1);
  }

  if (!('steps' in data)) {
    console.error(
      `There are no steps defined for ${language} in "${data.name}" (${data.id})`,
    );
    process.exit(1);
  }

  let convsersation = readConversationFile(conversationId) || {};

  if (language in convsersation) {
    console.error(`${language} is already defined for "${conversationId}"`);
    process.exit(1);
  }

  let {
    name: title,
    completionScreen: { content: completionContent } = {},
    steps,
  } = data;

  if (isLesson) {
    const { defaultHeadline__markdown, defaultContent__markdown, close } =
      lessonCompletedScreen[language];

    steps.push({
      screenType: 'editorialWithImage',
      title: defaultHeadline__markdown,
      content: completionContent || defaultContent__markdown,
      buttonText: close,
    });
  }

  const sections = steps.map(migrateStep(conversationId, isLesson));

  writeConversationFile(conversationId, {
    ...convsersation,
    [language]: {
      id: conversationId,
      alias: conversationAlias,
      type: 'lesson',
      root: 'section1',
      sections,
    },
  });

  return [
    {
      conversationId,
      title,
    },
  ];
};

const migrateLesson = (language, courseId, defaultLanguageData) => (lesson) => {
  const { id: lessonId } = lesson;
  /* eslint-disable no-unused-vars */
  const { steps, ...lessonWithoutSteps } = lesson;
  const { name: courseName } = defaultLanguageData;
  const { name: lessonName } = defaultLanguageData.lessons.find(
    ({ id }) => id === lessonId,
  );

  if (!lessonName) {
    console.error(
      `Can't find default language version of "${lessonName}" (${lessonId})`,
    );
    process.exit(1);
  }

  const conversationId = createConversationIdFromLesson(courseId, lessonId);
  const conversationAlias = `${courseName} - ${lessonName}`;

  return {
    ...lessonWithoutSteps,
    conversations: migrateSteps(
      language,
      conversationId,
      conversationAlias,
      lesson,
      true,
    ),
  };
};

const migrateLessons = (language, id, { lessons }, defaultLanguageData) =>
  lessons.map(migrateLesson(language, id, defaultLanguageData));

export { migrateLessons, migrateSteps };
