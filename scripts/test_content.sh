#!/bin/bash

set -euo pipefail

echo "Installing dependencies"
time yarn install --frozen-lockfile

echo "Test building content"
time yarn build

echo "Running tests"
time yarn test --coverage