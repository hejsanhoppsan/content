# 29k Content

**Welcome to the open source content repository of 29k!**

This is the content source of all the lessons, meditations, exercises and UI translations of the [29k.org app](https://29k.org/download).

## Contributions

We gladly accept contributions from you in merge requests directly to this repository or by propsing a change at https://cms.29k.org.

## Licensing

Everything in this repository is licensed under [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

This license lets you remix, adapt, and build upon our work even for commercial purposes, as long as you credit us and license out new creations under identical terms.

## Hiring

We are always looking for passionate and talented peopole to join our mission - [visit our careers page](https://careers.29k.org/).

## Technical documentation

Visit [SETUP.md](./SETUP.md) for technical details.
